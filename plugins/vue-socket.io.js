import io from 'socket.io-client'
import Vue from 'vue'
import VueSocketIO from 'vue-socket.io'
import toastr from 'toastr'

toastr.options = {
  closeButton: true,
  debug: false,
  progressBar: true,
}

export default ({ env }) => {
  const socket = io(env.WS_URL, {
    path: env.WS_PATH,
    // secure: true,
    transports: ['websocket'],
  })

  /* socket.on('connect', () => {
    toastr.success(socket.id, 'Socket connected!', {
      timeOut: 3000,
      positionClass: 'toast-top-right',
      progressBar: true,
      closeButton: true
    })
  }) */

  /* socket.on('connect_error', (error) => {
    toastr.error(error, 'socket connect status!', {
      timeOut: 3000,
      positionClass: 'toast-top-right',
      progressBar: true,
      closeButton: true,
    })
  }) */

  Vue.use(
    new VueSocketIO({
      debug: process.env.NODE_ENV === 'development',
      connection: socket,
    })
  )
}

import ApiDashboard from '@/core/services/dashboard'
import ApiAuth from '@/core/services/auth'
import ApiRbac from '@/core/services/rbac'
import ApiSetting from '@/core/services/setting'
import ApiSchedule from '@/core/services/schedule'
import ApiKiosk from '@/core/services/kiosk'
import ApiHistoryTaking from '@/core/services/history-taking'
import ApiExamination from '@/core/services/examination'

export default ({ $axios }, inject) => {
  const apiDashboard = ApiDashboard($axios)
  const apiAuth = ApiAuth($axios)
  const apiRbac = ApiRbac($axios)
  const apiSetting = ApiSetting($axios)
  const apiSchedule = ApiSchedule($axios)
  const apiKiosk = ApiKiosk($axios)
  const apiHistoryTaking = ApiHistoryTaking($axios)
  const apiExamination = ApiExamination($axios)

  inject('apiDashboard', apiDashboard)
  inject('apiAuth', apiAuth)
  inject('apiRbac', apiRbac)
  inject('apiSetting', apiSetting)
  inject('apiSchedule', apiSchedule)
  inject('apiKiosk', apiKiosk)
  inject('apiHistoryTaking', apiHistoryTaking)
  inject('apiExamination', apiExamination)
}

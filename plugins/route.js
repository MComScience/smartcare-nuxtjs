import NProgress from 'nprogress'

export default ({ app }) => {
  app.router.beforeResolve((to, from, next) => {
    if (to.name) {
      // document.body.classList.add('kt-page--loading')
      NProgress.start()
    }
    next()
  })

  app.router.afterEach(() => {
    /* setTimeout(() => {
      document.body.classList.remove('kt-page--loading')
    }, 300) */
    NProgress.done()
  })
}

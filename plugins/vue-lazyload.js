import Vue from 'vue'
import VueLazyload from 'vue-lazyload'
import errorImg from '@/assets/images/404.png'

Vue.use(VueLazyload)

// or with options
Vue.use(VueLazyload, {
  error: errorImg,
})

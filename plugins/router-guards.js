export default ({ app, store }) => {
  app.router.beforeResolve((to, from, next) => {
    if (to.name) {
      document.body.classList.add('page-loading')
    }
    next()
  })

  app.router.afterEach(() => {
    document.body.classList.remove('page-loading')
  })
}

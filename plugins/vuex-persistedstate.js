import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  // store default paths
  let paths = ['auth', 'config', 'htmlclass', 'i18n']

  const takingSkipPaths = ['loading', 'service_groups', 'service_points', 'doctors']
  const takingPaths = Object.keys(store.state.historyTaking)
    .filter((k) => !takingSkipPaths.includes(k))
    .map((k) => `historyTaking.${k}`)
  paths = paths.concat(takingPaths)

  const examinationSkipPaths = ['loading', 'service_groups', 'service_points', 'doctors']
  const examinationPaths = Object.keys(store.state.examination)
    .filter((k) => !examinationSkipPaths.includes(k))
    .map((k) => `examination.${k}`)
  paths = paths.concat(examinationPaths)

  createPersistedState({
    key: 'vuex-store',
    paths: paths,
  })(store)
}

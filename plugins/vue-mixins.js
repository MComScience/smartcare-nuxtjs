import Vue from 'vue'

Vue.mixin({
  methods: {
    setLoading: function (loading = false) {
      this.loading = loading
    },
    setSubmitted: function (submitted = false) {
      this.submitted = submitted
    },
  },
})

import Vue from 'vue'
import Loading from 'vue-loading-overlay'
import VueApexCharts from 'vue-apexcharts'
import CKEditor from 'ckeditor4-vue'
import VueTheMask from 'vue-the-mask'
import VueTimepicker from 'vue2-timepicker/src/vue-timepicker.vue'
import FlatIcon from '@/components/Icons/FlatIcon'
import DataTable from '@/components/Base/Table/DataTable'
import Card from '@/components/Base/Card/Card'
import Select2 from '@/components/Base/Input/Select2'
import VSelect from '@/components/Base/Input/VSelect'
import Modal from '@/components/Base/Modal/Modal'

Vue.component('v-loading', Loading)
Vue.component('apexchart', VueApexCharts)
Vue.component('flat-icon', FlatIcon)
Vue.component('data-table', DataTable)
Vue.component('card', Card)
Vue.component('select2', Select2)
Vue.component('v-select', VSelect)
Vue.component('modal', Modal)
Vue.component('vue-timepicker', VueTimepicker)
Vue.use(CKEditor)
Vue.use(VueTheMask)

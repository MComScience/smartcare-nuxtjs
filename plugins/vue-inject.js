import isEmpty from 'is-empty'
import {
  toQueryString,
  getErrorMessage,
  errorHandler,
  updateObject,
  groupBy,
  sortBy,
  mapDataOptions,
  xmlToJson,
} from '@/utils'
import { EVENTS } from '@/core/config/socket-events'

export default ({ app, env }, inject) => {
  inject('toQueryString', toQueryString)
  inject('getErrorMessage', getErrorMessage)
  inject('errorHandler', errorHandler)
  inject('updateObject', updateObject)
  inject('groupBy', groupBy)
  inject('sortBy', sortBy)
  inject('mapDataOptions', mapDataOptions)
  inject('isEmpty', isEmpty)
  inject('xmlToJson', xmlToJson)
  inject('SOCKET_EVENTS', EVENTS)
  inject('env', env)
}

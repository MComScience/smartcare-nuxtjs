import Vue from 'vue'
import Highcharts from 'highcharts'
import HighchartsVue from 'highcharts-vue'
import exportingInit from 'highcharts/modules/exporting'
import exportDataInit from 'highcharts/modules/export-data'
import drilldownInit from 'highcharts/modules/drilldown'

exportingInit(Highcharts)
exportDataInit(Highcharts)
drilldownInit(Highcharts)

Vue.use(HighchartsVue)

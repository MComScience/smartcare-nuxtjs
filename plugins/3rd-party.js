import Swal from 'sweetalert2'
import toastr from 'toastr'
import 'jquery'
import 'bootstrap'
import 'tooltip.js'
import '@popperjs/core'
import '@/core/plugins/metronic'

toastr.options = {
  closeButton: true,
  debug: false,
  progressBar: true,
}

export default ({ app }, inject) => {
  // Doc: https://sweetalert2.github.io/
  inject('Swal', Swal)
  // Doc: https://github.com/CodeSeven/toastr
  inject('toastr', toastr)
}

import _ from 'lodash'

export default ({ $axios, store, env }, inject) => {
  $axios.onRequest((config) => {
    const isMatchEdms = !!config.url.match(/edmsapi/)
    const isMatchHr = !!config.url.match(/hr/)
    const isMatchImed = !!config.url.match(/imed/)
    const token = store.state.auth.access_token
    if (isMatchHr) {
      config.headers.Authorization = `Bearer ${env.HR_TOKEN}`
    } else if (!isMatchEdms && !isMatchImed && token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  })

  $axios.interceptors.response.use(
    (response) => {
      if (!_.get(response, ['data']) || !_.get(response, ['data', 'data'])) {
        response = {
          data: response,
        }
      }
      return _.get(response, ['data'], response)
    },
    (error) => {
      if (error.response && error.response.data) {
        return Promise.reject(error.response.data)
      }
      return Promise.reject(error)
    }
  )
}

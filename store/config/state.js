import config from '@/core/config/layout.config.json'
import menus from '@/core/config/menu.config.json'

const state = () => ({
  config: config,
  menus: menus
})

export default state

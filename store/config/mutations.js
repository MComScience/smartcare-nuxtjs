import { SET_LAYOUT_CONFIG, OVERRIDE_LAYOUT_CONFIG } from './actions'

export default {
  [SET_LAYOUT_CONFIG](state, payload) {
    state.config = payload
  },
  [OVERRIDE_LAYOUT_CONFIG](state) {
    state.config = Object.assign({}, state.config, JSON.parse(localStorage.getItem('config')))
  }
}

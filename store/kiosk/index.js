import _ from 'lodash'
import qrCodeIcon from '@/assets/images/kiosk/qr-code.png'
import idCardIcon from '@/assets/images/kiosk/thai-id-card.png'
import employeeIcon from '@/assets/images/kiosk/travel.png'
import hnIcon from '@/assets/media/svg/avatars/004-boy-1.svg'
import {
  GET_SECTIONS,
  SET_LOADING,
  SET_SECTION,
  GET_CURRENT_SECTION_ID,
  SET_SEARCH_VALUE,
  SET_STATE_VALUE,
  TOGGLE_MODAL,
  GET_CURRENT_SECTION,
  SET_IP_ADDRESS,
  SET_PERSONAL_INFO,
  SET_SERVICE_GROUP_ITEMS,
  GET_PATIENT,
  SET_PATIENT,
  SET_PATIENT_RIGHT,
  GET_PATIENT_RIGHT,
  SET_SERVICE_GROUP_ID,
  SET_SERVICE_ID,
  SET_DOCTOR_ID,
  SET_DOCTOR_ITEMS,
  SET_APPOINT_ID,
  SET_DRAW_BLOOD,
  SET_X_RAY,
} from '@/core/types/kiosk'

export const state = () => ({
  loading: false,
  showModal: false,
  section: '', // ทำรายการด้วย
  search: '', // ทำรายการด้วย hn
  patient: null, // ข้อมูลผู้ป่วย
  personal_info: null, // ข้อมูลจากการอ่านบัตรประชาชน
  patient_right: null, // ข้อมูลสิทธิการรักษา
  ipAddress: '10.20.40.204',
  sections: [
    {
      label: 'สแกนคิวอาร์โค้ด (QR Code)',
      icon_url: qrCodeIcon,
      id: 'section-qrcode',
      _styles: {
        width: '8rem',
      },
    },
    {
      label: 'บัตรประชาชน',
      icon_url: idCardIcon,
      id: 'section-cid',
      _styles: {
        width: '11rem',
        bottom: '0px',
        right: '2rem',
      },
    },
    {
      label: 'สแกนบัตรพนักงาน',
      icon_url: employeeIcon,
      id: 'section-personal',
      _styles: {
        width: '8rem',
      },
    },
    {
      label: 'กรอกเลข HN หรือ เลขบัตรประชาชน',
      icon_url: hnIcon,
      id: 'section-hn',
      _styles: {
        width: '8rem',
      },
    },
  ],
  service_group_id: '', // ไอดีกลุ่มงานบริการ
  service_id: '', // ไอดีงานบริการ
  service_groups: [], // กลุ่มงานบริการ
  appoints: [], // รายการนัดหมาย
  doctor_id: '', // ไอดีแพทย์
  doctors: [], // รายชื่อแพทย์ตามตารางเวลาออกตรวจ
  appoint_id: null, // ไอดีนัดหมายที่เลือก
  draw_blood: false, // เจาะเลือด
  xray: false, // เอ็กซเรย์
})

export const getters = {
  [GET_SECTIONS](state) {
    return state.sections
  },
  [GET_CURRENT_SECTION](state) {
    return state.section
  },
  [GET_CURRENT_SECTION_ID](state) {
    return _.get(state, 'section.id', null)
  },
  [GET_PATIENT](state) {
    return _.get(state, 'patient', null)
  },
  [GET_PATIENT_RIGHT](state) {
    return _.get(state, 'patient_right', null)
  },
}

export const mutations = {
  [SET_LOADING](state, loading) {
    state.loading = loading
  },
  [SET_SECTION](state, section) {
    state.section = section
  },
  [SET_SEARCH_VALUE](state, value) {
    state.search = value
  },
  [SET_STATE_VALUE](state, payload) {
    state[payload.key] = payload.value
  },
  [TOGGLE_MODAL](state) {
    state.showModal = !state.showModal
  },
  [SET_IP_ADDRESS](state, ipAddress) {
    state.ipAddress = ipAddress
  },
  [SET_PERSONAL_INFO](state, payload) {
    state.personal_info = payload
  },
  [SET_SERVICE_GROUP_ITEMS](state, payload) {
    state.service_groups = payload
  },
  [SET_PATIENT](state, payload) {
    state.patient = payload
  },
  [SET_PATIENT_RIGHT](state, payload) {
    state.patient_right = payload
  },
  [SET_SERVICE_GROUP_ID](state, payload) {
    state.service_group_id = payload
  },
  [SET_SERVICE_ID](state, payload) {
    state.service_id = payload
  },
  [SET_DOCTOR_ID](state, payload) {
    state.doctor_id = payload
  },
  [SET_DOCTOR_ITEMS](state, payload) {
    state.doctors = payload
  },
  [SET_APPOINT_ID](state, payload) {
    state.appoint_id = payload
  },
  [SET_DRAW_BLOOD](state, payload) {
    state.draw_blood = payload
  },
  [SET_X_RAY](state, payload) {
    state.xray = payload
  },
}

export const actions = {
  setLoading({ commit }, payload) {
    commit(SET_LOADING, payload)
  },
  setSection({ commit }, section) {
    commit(SET_SECTION, section)
  },
  setSearchValue({ commit }, search) {
    commit(SET_SEARCH_VALUE, search)
  },
  setStateValue({ commit }, payload) {
    commit(SET_STATE_VALUE, payload)
  },
  toggleModal({ commit }) {
    commit(TOGGLE_MODAL)
  },
  setIpAddress({ commit }, payload) {
    commit(SET_IP_ADDRESS, payload)
  },
  setPersonalInfo({ commit }, payload) {
    commit(SET_PERSONAL_INFO, payload)
  },
  setServiceGroupItems({ commit }, payload) {
    commit(SET_SERVICE_GROUP_ITEMS, payload)
  },
  setPatient({ commit }, payload) {
    commit(SET_PATIENT, payload)
  },
  setPatientRight({ commit }, payload) {
    commit(SET_PATIENT_RIGHT, payload)
  },
  setServiceGroupId({ commit }, payload) {
    commit(SET_SERVICE_GROUP_ID, payload)
  },
  setServiceId({ commit }, payload) {
    commit(SET_SERVICE_ID, payload)
  },
  setDoctorId({ commit }, payload) {
    commit(SET_DOCTOR_ID, payload)
  },
  setDoctorItems({ commit }, payload) {
    commit(SET_DOCTOR_ITEMS, payload)
  },
  setAppointId({ commit }, payload) {
    commit(SET_APPOINT_ID, payload)
  },
  setDrawBlood({ commit }, payload) {
    commit(SET_DRAW_BLOOD, payload)
  },
  setXray({ commit }, payload) {
    commit(SET_X_RAY, payload)
  },
}

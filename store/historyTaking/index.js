import _ from 'lodash'
import {
  SET_LOADING,
  SET_CURRENT_TAB,
  SET_SERVICE_GROUP_ITEMS,
  SET_SERVICE_GROUP_ID,
  SET_SERVICE_IDS,
  SET_SERVICE_POINT_ID,
  SET_SERVICE_POINT_ITEMS,
  SET_CONFIRM_CALL_DIALOG,
  SET_CONFIRM_HOLD_DIALOG,
  SET_DOCTOR_ITEMS,
} from '@/core/types/history-taking'

export const state = () => ({
  showSetting: false,
  loading: false,
  currentTab: 'waiting',

  service_groups: [], // กลุ่มงานบริการ
  service_points: [], // จุดบริการ
  doctors: [], // รายชื่อแพทย์
  // data models
  service_group_id: '', // ไอดีกลุ่มงานบริการ
  service_ids: [], // งานบริการ
  service_point_id: '', // จุดบริการ

  confirmCallDialog: true, // ยืนยันการเรียกคิว
  confirmHoldDialog: true, // ยืนยันการพักคิว
})

export const getters = {
  getCurrentTab: (state) => {
    return state.currentTab
  },
  getServices: (state) => {
    const groups = state.service_groups.find((row) => row.service_group_id === state.service_group_id)
    return _.get(groups, 'services', [])
  },
}

export const mutations = {
  [SET_LOADING](state, loading) {
    state.loading = loading
  },
  [SET_CURRENT_TAB](state, payload) {
    state.currentTab = payload
  },
  [SET_SERVICE_GROUP_ITEMS](state, payload) {
    state.service_groups = payload
  },
  [SET_SERVICE_GROUP_ID](state, payload) {
    state.service_group_id = payload
  },
  [SET_SERVICE_IDS](state, payload) {
    state.service_ids = payload
  },
  [SET_SERVICE_POINT_ID](state, payload) {
    state.service_point_id = payload
  },
  [SET_SERVICE_POINT_ITEMS](state, payload) {
    state.service_points = payload
  },
  [SET_CONFIRM_CALL_DIALOG](state, payload) {
    state.confirmCallDialog = payload
  },
  [SET_CONFIRM_HOLD_DIALOG](state, payload) {
    state.confirmHoldDialog = payload
  },
  [SET_DOCTOR_ITEMS](state, payload) {
    state.doctors = payload
  },
}

export const actions = {
  setLoading({ commit }, payload) {
    commit(SET_LOADING, payload)
  },
  setCurrentTab({ commit }, payload) {
    commit(SET_CURRENT_TAB, payload)
  },
  setServiceGroupItems({ commit }, payload) {
    commit(SET_SERVICE_GROUP_ITEMS, payload)
  },
  setServiceGroupId({ commit }, payload) {
    commit(SET_SERVICE_GROUP_ID, payload)
  },
  setServiceIds({ commit }, payload) {
    commit(SET_SERVICE_IDS, payload)
  },
  setServicePointId({ commit }, payload) {
    commit(SET_SERVICE_POINT_ID, payload)
  },
  setServicePointItems({ commit }, payload) {
    commit(SET_SERVICE_POINT_ITEMS, payload)
  },
  setConfirmCallDialog({ commit }, payload) {
    commit(SET_CONFIRM_CALL_DIALOG, payload)
  },
  setConfirmHoldDialog({ commit }, payload) {
    commit(SET_CONFIRM_HOLD_DIALOG, payload)
  },
  setDoctorItems({ commit }, payload) {
    commit(SET_DOCTOR_ITEMS, payload)
  },
}

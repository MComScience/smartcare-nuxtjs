export default {
  /**
   * Get list of breadcrumbs for current page
   * @param state
   * @returns {*}
   */
  breadcrumbs(state) {
    return state.breadcrumbs
  },

  /**
   * Get the page title
   * @param state
   * @returns {*}
   */
  pageTitle(state) {
    const last = state.breadcrumbs[state.breadcrumbs.length - 1]
    if (last && last.title) {
      return last.title
    }
  }
}

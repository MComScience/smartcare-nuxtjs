import { APPEND_BREADCRUM, SET_BREADCRUMB } from './actions'

export default {
  [APPEND_BREADCRUM](state, payload) {
    state.breadcrumbs = [...state.breadcrumbs, payload]
  },
  [SET_BREADCRUMB](state, payload) {
    state.breadcrumbs = payload
  }
}

// action types
export const APPEND_BREADCRUM = 'appendBreadcrumb'

// mutation types
export const SET_BREADCRUMB = 'setBreadcrumb'
export const ADD_BREADCRUMB = 'addBreadcrumb'

export default {
  /**
   * Set the breadcrumbs list
   * @param state
   * @param payload
   */
  [SET_BREADCRUMB](state, payload) {
    state.commit(SET_BREADCRUMB, payload)
  },

  /**
   * Add breadcrumb
   * @param state
   * @param payload
   */
  [ADD_BREADCRUMB](state, payload) {
    if (typeof payload === 'object') {
      payload.forEach((item) => state.commit(APPEND_BREADCRUM, item))
    } else {
      state.commit(APPEND_BREADCRUM, payload)
    }
  }
}

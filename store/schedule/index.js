import {
  SET_SERVICE_GROUP_FILTER,
  SET_DOCTOR_FILTER,
  SET_FILTER_KEY,
  SET_FILTER_HOLIDAY_ONLY,
  SET_FILTER_DAYS,
  SET_HOLIDAY_ITEMS,
  SET_DATE_DISABLED,
  SET_SERVICE_ITEMS,
  SET_EVENTS_ITEMS,
  SET_SERVICE_POINT_ITEMS,
} from '@/core/types/schedule'

export const state = () => ({
  // filter keys
  filterKey: '',
  service_group_filter_key: '',
  doctor_filter_key: '',
  holiday_only: false,
  filter_days: [],

  datesDisabled: [],
  holidayEvents: [],
  // วันหยุด
  holidays: [],
  // งานบริการ
  services: [],
  // ข้อมูลตารางแพทย์
  events: [],
  // จุดบริการ
  service_points: [],
})

export const getters = {}

export const mutations = {
  [SET_SERVICE_GROUP_FILTER](state, payload) {
    state.service_group_filter_key = payload
  },
  [SET_DOCTOR_FILTER](state, payload) {
    state.doctor_filter_key = payload
  },
  [SET_FILTER_KEY](state, payload) {
    state.filterKey = payload
  },
  [SET_FILTER_HOLIDAY_ONLY](state, payload) {
    state.holiday_only = payload
  },
  [SET_FILTER_DAYS](state, payload) {
    state.filter_days = payload
  },
  [SET_HOLIDAY_ITEMS](state, payload) {
    state.holidays = payload
  },
  [SET_DATE_DISABLED](state, payload) {
    state.datesDisabled = payload
  },
  [SET_SERVICE_ITEMS](state, payload) {
    state.services = payload
  },
  [SET_EVENTS_ITEMS](state, payload) {
    state.events = payload
  },
  [SET_SERVICE_POINT_ITEMS](state, payload) {
    state.service_points = payload
  },
}

export const actions = {
  [SET_SERVICE_GROUP_FILTER]({ commit }, payload) {
    commit(SET_SERVICE_GROUP_FILTER, payload)
  },
  [SET_DOCTOR_FILTER]({ commit }, payload) {
    commit(SET_DOCTOR_FILTER, payload)
  },
}

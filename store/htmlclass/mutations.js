import _ from 'lodash'
import { SET_CLASSNAME_BY_POSITION } from './actions'

export default {
  [SET_CLASSNAME_BY_POSITION](state, payload) {
    const { position, className } = payload
    if (!state.classes[position]) {
      state.classes[position] = []
    }
    const positions = _.get(state.classes, [position], [])
    if (!positions.includes(className)) {
      state.classes[position].push(className)
    }
  },
}

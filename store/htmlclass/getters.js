export default {
  getClasses: (state) => (position) => {
    if (typeof position !== 'undefined') {
      return state.classes[position]
    }
    return state.classes
  }
}

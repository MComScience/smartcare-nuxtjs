export default {
  isAuthenticated(state) {
    return state.loggedIn
  },
  currentUser(state) {
    return state.user
  },
  expiresIn(state) {
    return state.expiresIn
  },
  refreshToken(state) {
    return state.refresh_token
  },
  userRoles(state) {
    return state.roles || []
  },
  userPermissions(state) {
    return state.permissions || []
  },
}

export default () => ({
  access_token: null,
  refresh_token: null,
  scope: null,
  token_type: null,
  user: null,
  loggedIn: false,
  expiresIn: null,
  auth_state: null,
  redirectUrl: null,
  roles: [],
  permissions: [],
})

import {
  SET_AUTH_STATE,
  SET_USER_DATA,
  SET_TOKEN_DATA,
  SET_USER_ROLES,
  SET_USER_PERMISSIONS,
  USER_LOGOUT,
} from '@/core/types/auth'

export default {
  [SET_AUTH_STATE](state, payload) {
    state.auth_state = payload
  },
  [SET_USER_DATA](state, payload) {
    state.user = payload
    state.loggedIn = true
  },
  [SET_USER_ROLES](state, payload) {
    state.roles = payload
  },
  [SET_USER_PERMISSIONS](state, payload) {
    state.permissions = payload
  },
  [SET_TOKEN_DATA](state, payload) {
    state.access_token = payload.access_token
    state.expiresIn = payload.expires_in
    state.token_type = payload.token_type
    if (payload.refresh_token) {
      state.refresh_token = payload.refresh_token
      state.scope = payload.scope
    }
  },
  [USER_LOGOUT](state) {
    state.access_token = null
    state.refresh_token = null
    state.scope = null
    state.token_type = null
    state.user = null
    state.loggedIn = false
    state.expiresIn = null
    state.auth_state = null
    state.redirectUrl = null
    state.roles = []
    state.permissions = []
  },
}

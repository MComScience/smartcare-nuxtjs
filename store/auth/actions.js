import Swal from 'sweetalert2'
import {
  SET_AUTH_STATE,
  SET_USER_DATA,
  SET_TOKEN_DATA,
  SET_USER_ROLES,
  SET_USER_PERMISSIONS,
  USER_LOGOUT,
  CHECK_AUTH_TIMEOUT,
} from '@/core/types/auth'

let timeout = null

export default {
  [SET_AUTH_STATE]({ commit }, state) {
    commit(SET_AUTH_STATE, state)
  },
  [SET_USER_DATA]({ commit }, payload) {
    commit(SET_USER_DATA, payload)
  },
  [SET_TOKEN_DATA]({ commit }, token) {
    commit(SET_TOKEN_DATA, token)
  },
  [SET_USER_ROLES]({ commit }, token) {
    commit(SET_USER_ROLES, token)
  },
  [SET_USER_PERMISSIONS]({ commit }, token) {
    commit(SET_USER_PERMISSIONS, token)
  },
  [USER_LOGOUT]({ commit }) {
    commit(USER_LOGOUT)
  },
  [CHECK_AUTH_TIMEOUT]({ commit }, payload) {
    const router = payload.router
    const currentTime = new Date().getTime()
    const expirationTime = new Date(payload.expiresIn * 1000).getTime()
    if (expirationTime < currentTime) {
      Swal.fire({
        icon: 'warning',
        title: 'เซสชั่นหมดอายุ',
        text: 'กรุณาเข้าสู่ระบบอีกครั้ง',
        allowOutsideClick: false,
        confirmButtonText: 'ตกลง',
      })
      commit(USER_LOGOUT)
      router.push('/auth/login')
    } else {
      if (timeout) {
        clearTimeout(timeout)
      }
      timeout = setTimeout(() => {
        Swal.fire({
          icon: 'warning',
          title: 'เซสชั่นหมดอายุ',
          text: 'กรุณาเข้าสู่ระบบอีกครั้ง',
          allowOutsideClick: false,
          confirmButtonText: 'ตกลง',
        })
        commit(USER_LOGOUT)
        router.push('/auth/login')
      }, payload.expiresIn)
    }
  },
}

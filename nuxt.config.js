const devMode = process.env.NODE_ENV === 'development'
const webpack = require('webpack')

// environment variables
const envDevPath = './.env.dev'
const envProdPath = './.env.production'
require('dotenv').config({ path: devMode ? envDevPath : envProdPath })

export default {
  /**
   * environment
   * https://nuxtjs.org/api/configuration-env
   */
  env: {
    baseUrl: process.env.NUXT_ENV_BASE_URL || 'http://localhost:3000',
    HR_TOKEN: process.env.HR_TOKEN,
    API_BASE_URL: process.env.API_BASE_URL,
    WS_URL: process.env.WS_URL,
    WS_PATH: process.env.WS_PATH,
    PRINT_BASE_URL: process.env.PRINT_BASE_URL,
  },
  /**
   * https://nuxtjs.org/api/configuration-server
   */
  server: {
    port: process.env.NUXT_ENV_PORT, // default: 3000
    host: process.env.NUXT_ENV_HOST, // default: localhost,
  },
  /**
   * https://nuxtjs.org/api/configuration-router
   */
  router: {
    // base: '/',
    middleware: 'breadcrumbs',
    // extendRoutes(routes, resolve) {
    //   routes.push({
    //     name: 'page not found',
    //     path: '*',
    //     component: resolve(__dirname, 'pages/error/404.vue'),
    //   })
    // },
  },
  /**
   * https://nuxtjs.org/api/configuration-transition
   */
  layoutTransition: {
    name: 'layout',
    mode: 'out-in',
  },
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'spa',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: 'EGAT Smart Care',
    titleTemplate: '%s - EGAT Smart Care',
    bodyAttrs: {
      class: ['header-fixed header-mobile-fixed sidebar-enabled'],
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
      {
        hid: 'description',
        name: 'description',
        content: 'ระบบคิว EGAT Smart Care',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'shortcut icon', href: '/favicon.ico' },
    ],
    script: [{ src: 'https://polyfill.io/v3/polyfill.min.js?features=IntersectionObserver', body: true }],
  },
  /*
   ** Global CSS
   */
  css: [
    '@/assets/plugins/global/plugins.bundle.css',
    '@/assets/css/style.bundle.min.css',
    'vue2-timepicker/dist/VueTimepicker.css',
    'toastr/build/toastr.min.css',
    '@/assets/css/main.min.css',
    '@fortawesome/fontawesome-free/css/all.min.css',
    '@fortawesome/fontawesome-svg-core/styles.css',
    '@/assets/css/nprogress.min.css',
    'animate.css/animate.min.css',
    '@/assets/css/fullcalendar.bundle.css',
    '@/assets/css/kiosk.min.css',
  ],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    // '~/plugins/router-guards.js',
    '~/plugins/vue-inject.js',
    '~/plugins/vue-mixins.js',
    '~/plugins/axios.js',
    '~/plugins/api-service.js',
    '~/plugins/route.js',
    '~/plugins/3rd-party.js',
    '~/plugins/vee-validate.js',
    '~/plugins/components.js',
    '~/plugins/fontawesome.js',
    '~/plugins/vue-lazyload.js',
    '~/plugins/highcharts-vue.js',
    '~/plugins/vue-socket.io.js',
    // { src: '~plugins/ga.js', mode: 'client' },
    { src: '~/plugins/vuex-persistedstate.js', mode: 'client' },
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt/content
    '@nuxt/content',
    // Doc: https://github.com/nuxt-community/nuxt-i18n
    [
      'nuxt-i18n',
      {
        locales: [
          {
            code: 'en',
            iso: 'en-US',
            name: 'English',
            file: 'en-US.js',
          },
          {
            code: 'th',
            iso: 'th-TH',
            name: 'ไทย',
            file: 'th-TH.js',
          },
        ],
        defaultLocale: 'th',
        seo: true,
        lazy: true,
        langDir: 'lang/',
        vueI18n: {
          fallbackLocale: 'th',
        },
      },
    ],
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.API_BASE_URL,
  },
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {},
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#42b983' },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    analyze: !devMode,
    plugins: [
      new webpack.ProvidePlugin({
        // global modules
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        _: 'lodash',
      }),
    ],
    transpile: ['vee-validate/dist/rules'],
    extend(config, { isClient }) {
      // Extend only webpack config for client-bundle
      if (isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true,
          },
        })
      }
    },
  },
}

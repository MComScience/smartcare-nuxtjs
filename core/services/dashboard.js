const ROUTE_PREFIX = '/queue/v1/dashboard'

export default (axios) => ({
  getDataCount() {
    return axios.$get(`${ROUTE_PREFIX}/count`)
  },
})

import * as utils from '@/utils'
const ROUTE_PREFIX = '/queue/v1/settings'
const QUEUE_STATUS = 'queue-status'
const QUEUE_TYPE = 'queue-type'
const TICKET = 'ticket'
const DOCTOR = 'doctor'
const SERVICE = 'service'
const SERVICE_POINT = 'service-point'
const SERVICE_GROUP = 'service-group'
const PLAYER_STATION = 'player-station'

const ROUTE_PREFIX_QUEUE_STATUS = `${ROUTE_PREFIX}/${QUEUE_STATUS}`
const ROUTE_PREFIX_QUEUE_TYPE = `${ROUTE_PREFIX}/${QUEUE_TYPE}`
const ROUTE_PREFIX_TICKET = `${ROUTE_PREFIX}/${TICKET}`
const ROUTE_PREFIX_DOCTOR = `${ROUTE_PREFIX}/${DOCTOR}`
const ROUTE_PREFIX_SERVICE = `${ROUTE_PREFIX}/${SERVICE}`
const ROUTE_PREFIX_SERVICE_POINT = `${ROUTE_PREFIX}/${SERVICE_POINT}`
const ROUTE_PREFIX_SERVICE_GROUP = `${ROUTE_PREFIX}/${SERVICE_GROUP}`
const ROUTE_PREFIX_PLAYER_STATION = `${ROUTE_PREFIX}/${PLAYER_STATION}`

export default (axios) => ({
  /* สถานะคิว */
  /**
   * ข้อมูลสถานะคิว /queue/v1/settings/queue-status
   * @method GET
   */
  getQueueStatus() {
    return axios.$get(`${ROUTE_PREFIX_QUEUE_STATUS}`)
  },

  /**
   * บันทึกสถานะคิว /queue/v1/settings/queue-status
   * @method PUT
   * @param {Object} body
   */
  createQueueStatus(body) {
    return axios.$put(`${ROUTE_PREFIX_QUEUE_STATUS}`, body)
  },

  /**
   * แก้ไขสถานะคิว /queue/v1/settings/queue-status/{id}
   * @method GET
   * @param {number} id
   */
  getQueueStatusById(id) {
    return axios.$get(`${ROUTE_PREFIX_QUEUE_STATUS}/${id}`)
  },

  /**
   * แก้ไขสถานะคิว /queue/v1/settings/queue-status/{id}
   * @method POST
   * @param {number} id
   * @param {Object} body
   */
  updateQueueStatus(id, body) {
    return axios.$post(`${ROUTE_PREFIX_QUEUE_STATUS}/${id}`, body)
  },

  /**
   * ลบรายการสถานะคิว /queue/v1/settings/queue-status/{id}
   * @method DELETE
   * @param {number} id
   */
  deleteQueueStatus(id) {
    return axios.$delete(`${ROUTE_PREFIX_QUEUE_STATUS}/${id}`)
  },

  /* ประเภทคิว */

  /**
   * ข้อมูลประเภทคิว /queue/v1/settings/queue-types
   * @method GET
   */
  getQueueTypes() {
    return axios.$get(`${ROUTE_PREFIX_QUEUE_TYPE}`)
  },

  /**
   * บันทึกรายการประเภทคิว /queue/v1/settings/queue-type
   * @method PUT
   * @param {Object} body
   */
  createQueueType(body) {
    return axios.$put(`${ROUTE_PREFIX_QUEUE_TYPE}`, body)
  },

  /**
   * แก้ไขรายการประเภทคิว /queue/v1/settings/queue-type/{id}
   * @method GET
   * @param {number} id
   */
  getQueueTypeById(id) {
    return axios.$get(`${ROUTE_PREFIX_QUEUE_TYPE}/${id}`)
  },

  /**
   * แก้ไขรายการประเภทคิว /queue/v1/settings/queue-type/{id}
   * @method POST
   * @param {number} id
   * @param {Object} body
   */
  updateQueueType(id, body) {
    return axios.$post(`${ROUTE_PREFIX_QUEUE_TYPE}/${id}`, body)
  },

  /**
   * ลบรายการประเภทคิว /queue/v1/settings/queue-type/{id}
   * @method DELETE
   * @param {number} id
   */
  deleteQueueType(id) {
    return axios.$delete(`${ROUTE_PREFIX_QUEUE_TYPE}/${id}`)
  },

  /* บัตรคิว  */

  /**
   * ข้อมูลบัตรคิว  /queue/v1/settings/tickets
   * @method GET
   */
  getAllTicket() {
    return axios.$get(`${ROUTE_PREFIX_TICKET}`)
  },
  /**
   * บันทึกรายการบัตรคิว /queue/v1/settings/ticket
   * @method PUT
   * @param {Object} body
   */
  createTicket(body) {
    return axios.$put(`${ROUTE_PREFIX_TICKET}`, body)
  },

  /**
   * แก้ไขรายการบัตรคิว /queue/v1/settings/ticket/{id}
   * @method GET
   * @param {number} id
   */
  getTicketById(id) {
    return axios.$get(`${ROUTE_PREFIX_TICKET}/${id}`)
  },

  /**
   * แก้ไขรายการบัตรคิว /queue/v1/settings/ticket/{id}
   * @method POST
   * @param {number} id
   * @param {Object} body
   */
  updateTicket(id, body) {
    return axios.$post(`${ROUTE_PREFIX_TICKET}/${id}`, body)
  },

  /**
   * ลบรายการบัตรคิว /queue/v1/settings/ticket/{id}
   * @method DELETE
   * @param {number} id
   */
  deleteTicket(id) {
    return axios.$delete(`${ROUTE_PREFIX_TICKET}/${id}`)
  },

  /* แพทย์ */
  /**
   * ข้อมูลแพทย์ /queue/v1/settings/doctors
   * @method GET
   */
  getDoctors() {
    return axios.$get(`${ROUTE_PREFIX_DOCTOR}`)
  },

  /**
   * บันทึกรายการแพทย์ /queue/v1/settings/doctor
   * @method PUT
   * @param {Object} body
   */
  createDoctor(body) {
    return axios.$put(`${ROUTE_PREFIX_DOCTOR}`, body)
  },

  /**
   * แก้ไขรายการแพทย์ /queue/v1/settings/doctor/{id}
   * @method GET
   * @param {number} id
   */
  getDoctorById(id) {
    return axios.$get(`${ROUTE_PREFIX_DOCTOR}/${id}`)
  },

  /**
   * แก้ไขรายการแพทย์ /queue/v1/settings/doctor/{id}
   * @method POST
   * @param {number} id
   * @param {Object} body
   */
  updateDoctor(id, body) {
    return axios.$post(`${ROUTE_PREFIX_DOCTOR}/${id}`, body)
  },

  /**
   * ลบรายการแพทย์ /queue/v1/settings/doctor/{id}
   * @method DELETE
   * @param {number} id
   */
  deleteDoctor(id) {
    return axios.$delete(`${ROUTE_PREFIX_DOCTOR}/${id}`)
  },

  /* งานบริการ */

  /**
   * ข้อมูลงานบริการ /queue/v1/settings/services
   * @method GET
   */
  getAllService() {
    return axios.$get(`${ROUTE_PREFIX_SERVICE}`)
  },

  /**
   * บันทึกงานบริการ /queue/v1/settings/service
   * @method PUT
   * @param {Object} body
   */
  createService(body) {
    return axios.$put(`${ROUTE_PREFIX_SERVICE}`, body)
  },

  /**
   * แก้ไขงานบริการ /queue/v1/settings/service/{id}
   * @method GET
   * @param {number} id
   */
  getServiceById(id) {
    return axios.$get(`${ROUTE_PREFIX_SERVICE}/${id}`)
  },

  /**
   * แก้ไขงานบริการ /queue/v1/settings/service/{id}
   * @method POST
   * @param {number} id
   * @param {Object} body
   */
  updateService(id, body) {
    return axios.$post(`${ROUTE_PREFIX_SERVICE}/${id}`, body)
  },

  /**
   * ลบงานบริการ /queue/v1/settings/service/{id}
   * @method DELETE
   * @param {number} id
   */
  deleteService(id) {
    return axios.$delete(`${ROUTE_PREFIX_SERVICE}/${id}`)
  },

  /* จุดบริการ */
  /**
   * รายชื่อจุดบริการ /queue/v1/settings/service-points
   * @method GET
   */
  getServicePoints() {
    return axios.$get(`${ROUTE_PREFIX_SERVICE_POINT}`)
  },

  /**
   * บันทึกรายการจุดบริการ /queue/v1/settings/service-point
   * @method PUT
   * @param {Object} body
   */
  createServicePoint(body) {
    return axios.$put(`${ROUTE_PREFIX_SERVICE_POINT}`, body)
  },

  /**
   * แก้ไขรายการจุดบริการ /queue/v1/settings/service-point/{id}
   * @method GET
   * @param {number} id
   */
  getServicePointById(id) {
    return axios.$get(`${ROUTE_PREFIX_SERVICE_POINT}/${id}`)
  },

  /**
   * แก้ไขรายการจุดบริการ /queue/v1/settings/service-point/{id}
   * @method POST
   * @param {number} id
   * @param {Object} body
   */
  updateServicePoint(id, body) {
    return axios.$post(`${ROUTE_PREFIX_SERVICE_POINT}/${id}`, body)
  },

  /**
   * ลบรายการจุดบริการ /queue/v1/settings/service-point/{id}
   * @method DELETE
   * @param {number} id
   */
  deleteServicePoint(id) {
    return axios.$delete(`${ROUTE_PREFIX_SERVICE_POINT}/${id}`)
  },

  /**
   * ไฟล์เสียงเรียก /queue/v1/settings/audio-file-list
   * @method GET
   */
  getAudioFileList() {
    return axios.$get(`${ROUTE_PREFIX}/audio-file`)
  },

  /* ตัวอักษรหน้าเลขคิว */
  /**
   * ข้อมูลตัวอักษรหน้าเลขคิว /queue/v1/settings/prefix
   * @method GET
   */
  getAllPrefix() {
    return axios.$get(`${ROUTE_PREFIX}/prefix`)
  },

  /**
   * ตัวเลือก /queue/v1/settings/prefix-options
   * @method GET
   */
  getPrefixOptions() {
    return axios.$get(`${ROUTE_PREFIX}/prefix-options`)
  },

  /**
   * บันทึกรายการตัวอักษร /queue/v1/settings/prefix
   * @method PUT
   * @param {Object} body
   */
  createPrefix(body) {
    return axios.$put(`${ROUTE_PREFIX}/prefix`, body)
  },

  /**
   * แก้ไขรายการตัวอักษร /queue/v1/settings/prefix/{id}
   * @method GET
   * @param {number} id
   */
  getPrefixById(id) {
    return axios.$get(`${ROUTE_PREFIX}/prefix/${id}`)
  },

  /**
   * แก้ไขรายการตัวอักษร /queue/v1/settings/prefix/{id}
   * @method POST
   * @param {number} id
   * @param {Object} body
   */
  updatePrefix(id, body) {
    return axios.$post(`${ROUTE_PREFIX}/prefix/${id}`, body)
  },

  /**
   * ลบรายการตัวอักษร /queue/v1/settings/prefix/{id}
   * @method DELETE
   * @param {number} id
   */
  deletePrefix(id) {
    return axios.$delete(`${ROUTE_PREFIX}/prefix/${id}`)
  },

  /**
   * บันทึกการจัดลำดับตัวอักษร /queue/v1/settings/save-order-prefix
   * @method POST
   * @param {Object} body
   */
  saveOrderPrefix(body) {
    return axios.$post(`${ROUTE_PREFIX}/save-order-prefix`, body)
  },

  /* กลุ่มงานบริการ */
  /**
   * รายชื่อกลุ่มงานบริการ /queue/v1/settings/service-groups
   * @method GET
   */
  getServiceGroups() {
    return axios.$get(`${ROUTE_PREFIX_SERVICE_GROUP}`)
  },

  /**
   * บันทึกรายการกลุ่มงานบริการ /queue/v1/settings/service-group
   * @method PUT
   * @param {Object} body
   */
  createServiceGroup(body) {
    return axios.$put(`${ROUTE_PREFIX_SERVICE_GROUP}`, body)
  },

  /**
   * แก้ไขกลุ่มงานบริการ /queue/v1/settings/service-group/{id}
   * @method GET
   * @param {number} id
   */
  getServiceGroupById(id) {
    return axios.$get(`${ROUTE_PREFIX_SERVICE_GROUP}/${id}`)
  },

  /**
   * แก้ไขกลุ่มงานบริการ  /queue/v1/settings/service-group/{id}
   * @method POST
   * @param {number} id
   * @param {Object} body
   */
  updateServiceGroup(id, body) {
    return axios.$post(`${ROUTE_PREFIX_SERVICE_GROUP}/${id}`, body)
  },

  /**
   * ลบรายการกลุ่มงานบริการ /queue/v1/settings/service-group/{id}
   * @method DELETE
   * @param {number} id
   */
  deleteServiceGroup(id) {
    return axios.$delete(`${ROUTE_PREFIX_SERVICE_GROUP}/${id}`)
  },

  /* โปรแกรมเสียงเรียกคิว */
  /**
   * ข้อมูลเสียงเรียกคิว /queue/v1/settings/player-stations
   * @method GET
   */
  getPlayerStationList() {
    return axios.$get(`${ROUTE_PREFIX_PLAYER_STATION}`)
  },

  /**
   * บันทึกรายการโปรแกรมเสียง /queue/v1/settings/player-station
   * @method PUT
   * @param {Object} body
   */
  createPlayerStation(body) {
    return axios.$put(`${ROUTE_PREFIX_PLAYER_STATION}`, body)
  },

  /**
   * แก้ไขรายการโปรแกรมเสียง /queue/v1/settings/player-station/{id}
   * @method GET
   * @param {number} id
   */
  getPlayerStationById(id) {
    return axios.$get(`${ROUTE_PREFIX_PLAYER_STATION}/${id}`)
  },

  /**
   * แก้ไขรายการโปรแกรมเสียง /queue/v1/settings/player-station/{id}
   * @method POST
   * @param {number} id
   * @param {Object} body
   */
  updatePlayerStation(id, body) {
    return axios.$post(`${ROUTE_PREFIX_PLAYER_STATION}/${id}`, body)
  },

  /**
   * ลบรายการโปรแกรมเสียง /queue/v1/settings/player-station/{id}
   * @method DELETE
   * @param {number} id
   */
  deletePlayerStation(id) {
    return axios.$delete(`${ROUTE_PREFIX_PLAYER_STATION}/${id}`)
  },

  /* ตารางแพทย์ */

  getDoctorScheduleEvents(params) {
    return axios.$get(`${ROUTE_PREFIX}/doctor-schedule${utils.toQueryString(params)}`)
  },
  getScheduleDoctorById(params) {
    return axios.$get(`${ROUTE_PREFIX}/doctor-schedule/${params.id}/sub-schedule-id/${params.sub_schedule_id}`)
  },
  createSchedule(body) {
    return axios.$put(`${ROUTE_PREFIX}/doctor-schedule`, body)
  },
  updateSchedule(id, body) {
    return axios.$post(`${ROUTE_PREFIX}/doctor-schedule/${id}`, body)
  },
  deleteScheduleDoctor(id) {
    return axios.$delete(`${ROUTE_PREFIX}/doctor-schedule/${id}`)
  },
  copyScheduleDoctor(id, body) {
    return axios.$post(`${ROUTE_PREFIX}/copy-doctor-schedule/${id}`, body)
  },
})

import * as utils from '@/utils'
const QUEUE_ROUTE_PREFIX = '/queue/v1'
const IMED_ROUTE_PREFIX = '/imed/portal-egat/api'
const HR_ROUTE_PREFIX = '/hr'

export default (axios) => ({
  getClientIP() {
    return axios.$get(`${QUEUE_ROUTE_PREFIX}/ip`)
  },
  // รายชื่อกลุ่มงานบริการ
  getServiceGroups() {
    return axios.$get(`${QUEUE_ROUTE_PREFIX}/kiosk/service-group`)
  },
  // ตรวจสอบข้อมูลผู้ป่วย
  getPatient(q) {
    return axios.$get(`${IMED_ROUTE_PREFIX}/register/checkPatient/${q}`)
  },
  // ตรวจสอบสิทธิ
  getRightForPatient(userId) {
    return axios.$get(`${HR_ROUTE_PREFIX}/queues/user/${userId}`)
  },
  // รายชื่อแพทย์ตามตางรางเวลาออกตรวจ
  getDoctors(query) {
    return axios.$get(`${QUEUE_ROUTE_PREFIX}/kiosk/doctors${utils.toQueryString(query)}`)
  },
  // open visit
  createPatientVisit(body) {
    return axios.$post(`${IMED_ROUTE_PREFIX}/register/createVisit`, body)
  },
  // ตรวจสอบเงื่อนไขก่อนออกบัตรคิว
  checkRegisterQueue(body) {
    return axios.$post(`${QUEUE_ROUTE_PREFIX}/kiosk/check-register`, body)
  },
  // สร้างรายการคิว
  registerQueue(body) {
    return axios.$post(`${QUEUE_ROUTE_PREFIX}/kiosk/register-queue`, body)
  },
  // ข้อมูลเช็คอินคิวอาร์โค้ด
  getCheckinData(qrcode) {
    return axios.$get(`${QUEUE_ROUTE_PREFIX}/kiosk/checkin/${qrcode}`)
  },
  checkinQueue(body) {
    return axios.$post(`${QUEUE_ROUTE_PREFIX}/kiosk/checkin`, body)
  },
  getServicePoints() {
    return axios.$get(`${QUEUE_ROUTE_PREFIX}/kiosk/service-point`)
  },
  // ตรวจสอบสิทธิ
  getIdCardByRFID(rfid) {
    return axios.$get(`${HR_ROUTE_PREFIX}/queues/IDCardByRFIDCode/${rfid}`)
  },
})

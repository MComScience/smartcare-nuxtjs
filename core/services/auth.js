const ROUTE_PREFIX = '/queue/v1'

export default (axios) => ({
  /**
   * เข้าสู่ระบบ /queue/v1/user/login
   * @method POST
   * @param {Object} body - username, password
   */
  login(body) {
    return axios.$post(`${ROUTE_PREFIX}/user/login`, body)
  },
})

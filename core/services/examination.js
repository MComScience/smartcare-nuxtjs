import * as utils from '@/utils'
const ROUTE_PREFIX = '/queue/v1/examination'

export default (axios) => ({
  getDataWaiting(params) {
    return axios.$get(`${ROUTE_PREFIX}/data-waiting${utils.toQueryString(params)}`)
  },
  getDataCalling(params) {
    return axios.$get(`${ROUTE_PREFIX}/data-calling${utils.toQueryString(params)}`)
  },
  getQueueList(params) {
    return axios.$get(`${ROUTE_PREFIX}/queue-list${utils.toQueryString(params)}`)
  },
  getDataQueueDoctor(params) {
    return axios.$get(`${ROUTE_PREFIX}/data-queue-doctor${utils.toQueryString(params)}`)
  },
  getDoctors() {
    return axios.$get(`${ROUTE_PREFIX}/doctors`)
  },
  getDoctorSchedule(params) {
    return axios.$get(`${ROUTE_PREFIX}/doctor-schedule${utils.toQueryString(params)}`)
  },

  callWaiting(body) {
    return axios.$post(`${ROUTE_PREFIX}/call-waiting`, body)
  },
  recall(body) {
    return axios.$post(`${ROUTE_PREFIX}/recall`, body)
  },
  hold(body) {
    return axios.$post(`${ROUTE_PREFIX}/hold`, body)
  },
  end(body) {
    return axios.$post(`${ROUTE_PREFIX}/end`, body)
  },
  deleteQueue(id) {
    return axios.$delete(`${ROUTE_PREFIX}/queue/${id}`)
  },
})

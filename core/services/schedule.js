import * as utils from '@/utils'

export default (axios) => ({
  /**
   * วันหยุดประจำปี /edms/public/api/edmsapi.php?{action}&{sdate}&{edate}
   * @method GET
   * @param {Object} params
   */
  getHolidays(params) {
    return axios.$get(decodeURIComponent(`/edms/public/api/edmsapi.php${utils.toQueryString(params)}`))
  },
  /**
   * copy ข้อมูลตารางแพทย์ /queue/v1/settings/copy-schedule-doctor-selected
   * @method POST
   * @param {Object} body
   */
  copySchedule(body) {
    return axios.$post(`/queue/v1/settings/copy-doctor-schedule-selected`, body)
  },
})

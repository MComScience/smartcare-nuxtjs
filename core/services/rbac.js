import querystring from 'querystring'
const ROUTE_PREFIX = '/queue/v1/rbac'

export default (axios) => ({
  /**
   * รายชื่อผู้ใช้งาน /queue/v1/rbac/assignments
   * @method GET
   */
  getUserAssignment() {
    return axios.$get(`${ROUTE_PREFIX}/assignments`)
  },
  /**
   * ข้อมูลสิทธิ์ /setting/rbac/assignment/view/{id}
   * @method GET
   * @param {number} userId
   */
  getAssignmentView(userId) {
    return axios.$get(decodeURIComponent(`${ROUTE_PREFIX}/assignment/view/${userId}`))
  },
  /**
   * /setting/rbac/assignment/assign-assignment{query}
   * @method POST
   * @param {Object} data
   * @param {string} userId
   */
  assignAssignment(data, userId) {
    const query = querystring.stringify({ id: userId })
    return axios.$post(`${ROUTE_PREFIX}/assign-assignment?${query}`, data)
  },
  /**
   * /setting/rbac/assignment/revoke-assignment{query}
   * @method POST
   * @param {Object} data
   * @param {string} userId
   */
  revokeAssignment(data, userId) {
    const query = querystring.stringify({ id: userId })
    return axios.$post(`${ROUTE_PREFIX}/revoke-assignment?${query}`, data)
  },
  /**
   * Roles
   */
  getRoles() {
    return axios.$get(`${ROUTE_PREFIX}/roles`)
  },
  createRole(body) {
    return axios.$put(`${ROUTE_PREFIX}/role`, body)
  },
  updateRole(body) {
    return axios.$post(decodeURIComponent(`${ROUTE_PREFIX}/role/${body.name}`), body)
  },
  deleteRole(id) {
    return axios.$delete(`${ROUTE_PREFIX}/role/${id}`)
  },
  getUpdateRole(id) {
    return axios.$get(`${ROUTE_PREFIX}/role/${id}`)
  },
  getViewRole(id) {
    return axios.$get(`${ROUTE_PREFIX}/role/view/${id}`)
  },
  assignRole(data, id) {
    return axios.$post(`${ROUTE_PREFIX}/assign-role?id=${id}`, data)
  },
  removeRole(data, id) {
    return axios.$post(`${ROUTE_PREFIX}/remove-role?id=${id}`, data)
  },
  /**
   * Permissions
   */
  getPermissions() {
    return axios.$get(`${ROUTE_PREFIX}/permissions`)
  },
  createPermission(body) {
    return axios.$put(`${ROUTE_PREFIX}/permission`, body)
  },
  getUpdatePermission(id) {
    return axios.$get(decodeURIComponent(`${ROUTE_PREFIX}/permission/${id}`))
  },
  updatePermission(id, body) {
    return axios.$post(decodeURIComponent(`${ROUTE_PREFIX}/permission/${id}`), body)
  },
  deletePermission(id) {
    return axios.$delete(decodeURIComponent(`${ROUTE_PREFIX}/permission/${id}`))
  },
  getViewPermission(id) {
    return axios.$get(decodeURIComponent(`${ROUTE_PREFIX}/permission/view/${id}`))
  },
  assignPermission(data, id) {
    return axios.$post(decodeURIComponent(`${ROUTE_PREFIX}/assign-permission?id=${id}`), data)
  },
  removePermission(data, id) {
    return axios.$post(decodeURIComponent(`${ROUTE_PREFIX}/remove-permission?id=${id}`), data)
  },
  /**
   * Routes
   */
  getRoutes() {
    return axios.$get(`${ROUTE_PREFIX}/routes`)
  },
  assignRoute(data) {
    return axios.$post(`${ROUTE_PREFIX}/assign-route`, data)
  },
  removeRoute(data) {
    return axios.$post(`${ROUTE_PREFIX}/remove-route`, data)
  },
})

export const ticketTemplate = `<div class="x_content">
<div class="row" style="margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px">
  <div class="col-sm-12">
    <p style="text-align:center">
      <span style="font-size:14px"><strong>EGAT Smart Care</strong> </span>
    </p>
  </div>
</div>

<div class="row" style="margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px">
  <div class="col-sm-6">
    <p style="text-align:left;"><strong>คิวของคุณ</strong></p>
    <span style="font-size: 26px;">
      <strong> {queue} </strong>
    </span>
  </div>
  <div class="col-sm-6" style="text-align:right;">
    <p style="text-align:right;">
      <small>
        ใช้ยืนยันตัวตน
      </small>
    </p>
    <img src="{img}" alt="" />
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <p class="font-weight-bold" style="font-size: 14px;font-weigth: bold;margin-bottom: 0;">
      <strong>ชื่อ-สกุล:</strong> {name}
    </p>
    <p class="font-weight-bold" style="font-size: 14px;font-weigth: bold;margin-bottom: 0;">
      <strong>HN:</strong> - {hn}
    </p>
    <br />
    <p class="font-weight-bold" style="font-size: 14px;font-weigth: bold;margin-bottom: 0;">
      <strong>รับบริการ:</strong> - {service_name}
    </p>
    <p class="font-weight-bold" style="font-size: 14px;font-weigth: bold;margin-bottom: 0;">
      <strong>แพทย์ที่พบ:</strong> - {doctor_name}
    </p>
    <p class="font-weight-bold" style="font-size: 14px;font-weigth: bold;margin-bottom: 0;">
      <strong>เจาะเลือด:</strong> - {draw_blood}
    </p>
    <p class="font-weight-bold" style="font-size: 14px;font-weigth: bold;margin-bottom: 0;">
      <strong>เอ็กซเรย์:</strong> - {xray}
    </p>
  </div>
</div>

<div class="row">
  <div class="col-xs-12 text-center">
    <p style="margin-left:0px; margin-right:0px; text-align:center">
      {date}
    </p>
  </div>
</div>
</div>`

export const profileHistory = `<div class="card card-custom">
<!--begin::Body-->
<div class="card-body pt-4">
  <!--begin::User-->
  <div class="d-flex align-items-end mb-7">
    <!--begin::Pic-->
    <div class="d-flex align-items-center">
      <!--begin::Pic-->
      <div class="flex-shrink-0 mr-4 mt-lg-0 mt-3">
        <div class="symbol symbol-circle symbol-lg-75">
          <img src="{img}" alt="image" />
        </div>
      </div>
      <!--end::Pic-->
      <!--begin::Title-->
      <div class="d-flex flex-column">
        <a href="#" class="text-dark font-weight-bold text-hover-primary font-size-h4 mb-0">
          {fullname}
        </a>
        <span class="text-muted font-weight-bold"></span>
      </div>
      <!--end::Title-->
    </div>
    <!--end::Title-->
  </div>
  <!--end::User-->
  <!--begin::Info-->
  <div class="mb-7">
    <div class="d-flex justify-content-between align-items-center">
      <span class="text-dark-75 font-weight-bolder mr-2">HN:</span>
      <span class="text-dark text-hover-primary font-weight-bold">{hn}</span>
    </div>
    <div class="d-flex justify-content-between align-items-cente my-1">
      <span class="text-dark-75 font-weight-bolder mr-2">กลุ่มงานบริการ:</span>
      <span class="text-dark text-hover-primary font-weight-bold">
        {service_group_name}
      </span>
    </div>
    <div class="d-flex justify-content-between align-items-center">
      <span class="text-dark-75 font-weight-bolder mr-2">ช่องบริการ:</span>
      <span class="text-dark font-weight-bold">
        {service_point_name}
      </span>
    </div>
    <div class="d-flex justify-content-between align-items-center">
      <span class="text-dark-75 font-weight-bolder mr-2">เจาะเลือดวันนี้:</span>
      <span class="text-dark font-weight-bold">
        {draw_blood_description}
      </span>
    </div>
    <div class="d-flex justify-content-between align-items-center">
      <span class="text-dark-75 font-weight-bolder mr-2">เอ็กซเรย์วันนี้:</span>
      <span class="text-dark font-weight-bold">
        {xray_description}
      </span>
    </div>
  </div>
  <!--end::Info-->
</div>
<!--end::Body-->
</div>`

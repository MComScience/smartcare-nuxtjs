export const EVENTS = {
  REGISTER: 'register',
  CALL_QUEUE: 'call queue',
  HOLD_QUEUE: 'hold queue',
  END_QUEUE: 'end queue',
  WAITING_DOCOTR_QUEUE: 'waiting doctor queue',
  SEND_DATA: 'send data',

  // สถานะเครื่องอ่านบัตรกำลังเชื่อมต่อ
  DEVICE_CONNECTED: 'DEVICE_CONNECTED',
  // เครื่องอ่านบัตร ปชช ไม่เชื่อมต่อ
  DEVICE_DISCONNECTED: 'DEVICE_DISCONNECTED',
  // เสียบบัตร ปชช
  CARD_INSERTED: 'CARD_INSERTED',
  // ถอดบัตร ปชช
  CARD_REMOVED: 'CARD_REMOVED',
  // เริ่มอ่านบัตร ปชช
  READING_START: 'READING_START',
  // กำลังอ่านข้อมูลบัตร ปชช
  READING_PROGRESS: 'READING_PROGRESS',
  // อ่านบัตร ปชช สำเร็จ
  READING_COMPLETE: 'READING_COMPLETE',
  // อ่านบัตร ปชช ไม่สำเร็จ
  READING_FAIL: 'READING_FAIL',

  SCREEN_FINISH: 'screen finish',
  DOCTOR_FINISH: 'doctor finish',
  PAID_FINISH: 'paid finish',
  DISPENSING_FINISH: 'dispensing finish',

  PRINTING_QUEUE: 'printing queue',
  AUDIO_PLAYING: 'audio playing',
  AUDIO_ENDED: 'audio ended',
  CANCEL_QUEUE: 'cancel queue',
}

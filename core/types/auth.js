// action types
export const AUTH_MODULE_NAME = 'auth'
export const SET_AUTH_STATE = 'setAuthState'
export const SET_USER_DATA = 'setUserData'
export const SET_TOKEN_DATA = 'setTokenData'
export const SET_USER_ROLES = 'setUserRoles'
export const SET_USER_PERMISSIONS = 'setUserPermissions'
export const USER_LOGOUT = 'userLogout'
export const CHECK_AUTH_TIMEOUT = 'checkAuthTimeout'

// Keenthemes plugins
import PerfectScrollbar from 'perfect-scrollbar'

import KTUtil from '@/assets/js/components/util.js'

import KTCard from '@/assets/js/components/card.js'

import KTCookie from '@/assets/js/components/cookie.js'

import KTDialog from '@/assets/js/components/dialog.js'

import KTHeader from '@/assets/js/components/header.js'

import KTImageInput from '@/assets/js/components/image-input.js'

import KTMenu from '@/assets/js/components/menu.js'

import KTOffcanvas from '@/assets/js/components/offcanvas.js'

import KTScrolltop from '@/assets/js/components/scrolltop.js'

import KTToggle from '@/assets/js/components/toggle.js'

import KTWizard from '@/assets/js/components/wizard.js'

// Metronic layout base js
import KTLayoutAside from '@/assets/js/layout/base/aside.js'

// import KTLayoutAsideMenu from '@/assets/js/layout/base/aside-menu.js'
// window.KTLayoutAsideMenu = KTLayoutAsideMenu

import KTLayoutContent from '@/assets/js/layout/base/content.js'

import KTLayoutFooter from '@/assets/js/layout/base/footer.js'

import KTLayoutHeader from '@/assets/js/layout/base/header.js'

import KTLayoutHeaderMenu from '@/assets/js/layout/base/header-menu.js'

import KTLayoutHeaderTopbar from '@/assets/js/layout/base/header-topbar.js'

// import KTLayoutSubheader from '@/assets/js/layout/base/subheader.js'
// window.KTLayoutSubheader = KTLayoutSubheader

// Metronic layout extended js
import KTLayoutChat from '@/assets/js/layout/extended/chat.js'

import KTLayoutExamples from '@/assets/js/layout/extended/examples.js'

import KTLayoutQuickActions from '@/assets/js/layout/extended/quick-actions.js'

import KTLayoutQuickCartPanel from '@/assets/js/layout/extended/quick-cart.js'

import KTLayoutQuickNotifications from '@/assets/js/layout/extended/quick-notifications.js'

import KTLayoutQuickPanel from '@/assets/js/layout/extended/quick-panel.js'

import KTLayoutQuickSearch from '@/assets/js/layout/extended/quick-search.js'

import KTLayoutQuickUser from '@/assets/js/layout/extended/quick-user.js'

import KTLayoutScrolltop from '@/assets/js/layout/extended/scrolltop.js'

import KTLayoutSearch from '@/assets/js/layout/extended/search.js'

import KTLayoutStickyCard from '@/assets/js/layout/extended/sticky-card.js'

import KTLayoutStretchedCard from '@/assets/js/layout/extended/stretched-card.js'

import KTApp from '@/assets/js/components/app.js'

const AppSettings = {
  breakpoints: { sm: 576, md: 768, lg: 992, xl: 1200, xxl: 1200 },
  colors: {
    theme: {
      base: {
        white: '#ffffff',
        primary: '#8950FC',
        secondary: '#E5EAEE',
        success: '#1BC5BD',
        info: '#8950FC',
        warning: '#FFA800',
        danger: '#F64E60',
        light: '#F3F6F9',
        dark: '#212121',
      },
      light: {
        white: '#ffffff',
        primary: '#E1E9FF',
        secondary: '#ECF0F3',
        success: '#C9F7F5',
        info: '#EEE5FF',
        warning: '#FFF4DE',
        danger: '#FFE2E5',
        light: '#F3F6F9',
        dark: '#D6D6E0',
      },
      inverse: {
        white: '#ffffff',
        primary: '#ffffff',
        secondary: '#212121',
        success: '#ffffff',
        info: '#ffffff',
        warning: '#ffffff',
        danger: '#ffffff',
        light: '#464E5F',
        dark: '#ffffff',
      },
    },
    gray: {
      'gray-100': '#F3F6F9',
      'gray-200': '#ECF0F3',
      'gray-300': '#E5EAEE',
      'gray-400': '#D6D6E0',
      'gray-500': '#B5B5C3',
      'gray-600': '#80808F',
      'gray-700': '#464E5F',
      'gray-800': '#1B283F',
      'gray-900': '#212121',
    },
  },
  'font-family': 'Prompt',
}
function fullscreen() {
  const element = document.documentElement
  if (!$('body').hasClass('full-screen')) {
    $('body').addClass('full-screen')
    if (element.requestFullscreen) {
      element.requestFullscreen()
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen()
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen()
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen()
    }
  } else {
    $('body').removeClass('full-screen')

    if (document.exitFullscreen) {
      document.exitFullscreen()
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen()
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen()
    }
  }
}
window.AppSettings = AppSettings
window.KTApp = KTApp
window.PerfectScrollbar = PerfectScrollbar
window.KTUtil = KTUtil
window.KTCard = KTCard
window.KTCookie = KTCookie
window.KTDialog = KTDialog
window.KTHeader = KTHeader
window.KTImageInput = KTImageInput
window.KTMenu = KTMenu
window.KTOffcanvas = KTOffcanvas
window.KTScrolltop = KTScrolltop
window.KTToggle = KTToggle
window.KTWizard = KTWizard
window.KTLayoutAside = KTLayoutAside
window.KTLayoutContent = KTLayoutContent
window.KTLayoutFooter = KTLayoutFooter
window.KTLayoutHeader = KTLayoutHeader
window.KTLayoutHeaderMenu = KTLayoutHeaderMenu
window.KTLayoutHeaderTopbar = KTLayoutHeaderTopbar
window.KTLayoutChat = KTLayoutChat
window.KTLayoutExamples = KTLayoutExamples
window.KTLayoutQuickActions = KTLayoutQuickActions
window.KTLayoutQuickCartPanel = KTLayoutQuickCartPanel
window.KTLayoutQuickNotifications = KTLayoutQuickNotifications
window.KTLayoutQuickPanel = KTLayoutQuickPanel
window.KTLayoutQuickSearch = KTLayoutQuickSearch
window.KTLayoutQuickUser = KTLayoutQuickUser
window.KTLayoutScrolltop = KTLayoutScrolltop
window.KTLayoutSearch = KTLayoutSearch
window.KTLayoutStickyCard = KTLayoutStickyCard
window.KTLayoutStretchedCard = KTLayoutStretchedCard
window.datepicker = require('bootstrap-datepicker')
window.nestable = require('@/assets/js/components/jquery.nestable')
window.fullscreen = fullscreen
window.timepicker = require('bootstrap-timepicker')
window.TouchSpin = require('bootstrap-touchspin')

$.fn.timepicker.defaults = $.extend(true, {}, $.fn.timepicker.defaults, {
  icons: {
    up: 'ki ki-arrow-up',
    down: 'ki ki-arrow-down',
  },
})

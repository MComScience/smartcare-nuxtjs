import _ from 'lodash'
import { mapActions, mapState } from 'vuex'
import Swal from 'sweetalert2'
import picture from '@/assets/media/svg/avatars/001-boy.svg'
import * as template from '@/core/template'

const mixins = {
  computed: {
    ...mapState({
      currentTab: (state) => _.get(state.historyTaking, 'currentTab', 'waiting'),
      serviceGroups: (state) => _.get(state.historyTaking, 'service_groups', []),
      service_group_id: (state) => _.get(state.historyTaking, 'service_group_id', ''),
      service_ids: (state) => _.get(state.historyTaking, 'service_ids', []),
      service_point_id: (state) => _.get(state.historyTaking, 'service_point_id', ''),
      servicePoints: (state) => _.get(state.historyTaking, 'service_points', []),
      doctors: (state) => _.get(state.historyTaking, 'doctors', []),
      confirmCallDialog: (state) => _.get(state.historyTaking, 'confirmCallDialog', false),
      confirmHoldDialog: (state) => _.get(state.historyTaking, 'confirmHoldDialog', false),
      user: (state) => _.get(state.auth, 'user', null),
    }),
    currentTabComponent() {
      return 'tab-' + this.currentTab.toLowerCase()
    },
    filteredData() {
      let data = []
      switch (this.currentTab) {
        case 'waiting':
          data = this.rowsWait
          break
        case 'call':
          data = this.rowsCall
          break
        case 'hold':
          data = this.rowsHold
          break
        case 'queue':
          data = this.rowsQueue
          break
        case 'queue-doctor':
          data = this.rowsDoctor
          break
        case 'waiting-doctor':
          data = this.rowsWaitingDoctor
          break
        default:
          data = []
          break
      }
      return data
    },
    countDataItems() {
      return {
        waiting: this.rowsWait.length,
        call: this.rowsCall.length,
        hold: this.rowsHold.length,
        'queue-list': this.rowsQueue.length,
        'queue-doctor': this.rowsDoctor.length,
        'waiting-doctor': this.rowsWaitingDoctor.length,
      }
    },
    // กลุ่มงานบริการที่เลือกตามการตั้งค่า
    serviceGroup() {
      return this.serviceGroups.find((item) => item.service_group_id === this.service_group_id)
    },
    // ชื่อกลุ่มงานบริการ
    serviceGroupName() {
      return _.get(this.serviceGroup, 'service_group_name', '')
    },
    // จุดบริการที่เลือกตามการตั้งค่า
    servicePoint() {
      return this.servicePoints.find((item) => item.service_point_id === this.service_point_id)
    },
    // ชื่อจุดบริการ
    servicePointName() {
      return _.get(this.servicePoint, 'service_point_description', '')
    },
    // รายการคิว lab
    filteredLabs() {
      return this.rowsQueue.filter((row) => row.queue_status_id === 7)
    },
    // รายการ visit id list (เฉพาะคิว lab)
    labVisitIdList() {
      return this.filteredLabs.map((row) => row.visit_id)
    },
  },
  beforeMount() {
    // ลงทะเบียนคิว
    const EVENTS = this.$SOCKET_EVENTS
    const sockets = this.sockets.listener
    sockets.subscribe(EVENTS.REGISTER, this.socketOnRegisterQueue)
    sockets.subscribe(EVENTS.CALL_QUEUE, this.socketOnCallQueue)
    sockets.subscribe(EVENTS.HOLD_QUEUE, this.socketOnHoldQueue)
    sockets.subscribe(EVENTS.END_QUEUE, this.socketOnEndQueue)
    sockets.subscribe(EVENTS.SCREEN_FINISH, this.socketOnScreenFinish)
    sockets.subscribe(EVENTS.AUDIO_PLAYING, this.socketOnAudioPlaying)
    sockets.subscribe(EVENTS.CANCEL_QUEUE, this.socketOnCancelQueue)
    sockets.subscribe(EVENTS.WAITING_DOCOTR_QUEUE, this.socketOnWaitingDoctor)
  },
  beforeDestroy() {
    const EVENTS = this.$SOCKET_EVENTS
    const sockets = this.sockets.listener
    sockets.unsubscribe(EVENTS.REGISTER)
    sockets.unsubscribe(EVENTS.CALL_QUEUE)
    sockets.unsubscribe(EVENTS.HOLD_QUEUE)
    sockets.unsubscribe(EVENTS.END_QUEUE)
    sockets.unsubscribe(EVENTS.SCREEN_FINISH)
    sockets.unsubscribe(EVENTS.AUDIO_PLAYING)
    sockets.unsubscribe(EVENTS.CANCEL_QUEUE)
    sockets.unsubscribe(EVENTS.WAITING_DOCOTR_QUEUE)
    if (this.intervalCheckLab) {
      clearInterval(this.intervalCheckLab)
    }
  },
  mounted() {
    this.fetchData()
    this.intervalCheckLab = setInterval(this.checkListReportLab, 60 * 1000 * 3) // เรียกผลแลบทุก 3 นาที
  },
  methods: {
    async fetchData() {
      await this.fetchDataServiceGroups()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataServicePoints()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataDoctors()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataWaiting()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataCalling()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataHold()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataQueueList()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataQueueDoctor()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataWaitingDoctor()
    },
    ...mapActions({
      setCurrentTab: 'historyTaking/setCurrentTab',
      setServiceGroupItems: 'historyTaking/setServiceGroupItems',
      setServicePointItems: 'historyTaking/setServicePointItems',
      setDoctorItems: 'historyTaking/setDoctorItems',
    }),
    // กลุ่มงานบริการ
    async fetchDataServiceGroups() {
      const _this = this
      if (_this.serviceGroups.length) return
      try {
        _this.setLoading(true)
        const serviceGroups = await _this.$apiKiosk.getServiceGroups()
        _this.setServiceGroupItems(serviceGroups)
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // ข้อมูลรายชื่อจุดบริการ
    async fetchDataServicePoints() {
      const _this = this
      if (_this.servicePoints.length) return
      try {
        _this.setLoading(true)
        const items = await _this.$apiKiosk.getServicePoints()
        _this.setServicePointItems(items)
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // คิวรอเรียก
    async fetchDataWaiting(newParams = {}) {
      const _this = this
      if (!_this.service_point_id || !_this.service_ids.length) return
      try {
        _this.setLoading(true)
        let params = {
          service_group_id: this.service_group_id,
          service_ids: this.service_ids,
        }
        params = _this.$updateObject(params, newParams)
        const data = await _this.$apiHistoryTaking.getDataWaiting(params)
        _this.rowsWait = data
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // คิวกำลังเรียก
    async fetchDataCalling(params = {}) {
      const _this = this
      if (!_this.isReadySetting) return
      try {
        _this.setLoading(true)
        const defaultParams = {
          service_group_id: _this.service_group_id,
          service_ids: _this.service_ids,
          service_point_id: _this.service_point_id,
          queue_status_id: 2, // default คิวกำลังเรียก
        }
        const dataParams = _this.$updateObject(defaultParams, params)
        const data = await _this.$apiHistoryTaking.getDataCalling(dataParams)

        // กำลังเรียก
        if (_.get(dataParams, ['caller_id'])) {
          _this.rowsCall = _this.rowsCall.concat(data)
        } else {
          _this.rowsCall = data
        }
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // คิวรอพบแพทย์
    async fetchDataWaitingDoctor(params = {}) {
      const _this = this
      if (!_this.isReadySetting) return
      try {
        _this.setLoading(true)
        const defaultParams = {
          service_group_id: _this.service_group_id,
          service_ids: _this.service_ids,
          service_point_id: _this.service_point_id,
          queue_status_id: 10, // default รอพบแพทย์
        }
        const dataParams = _this.$updateObject(defaultParams, params)
        const data = await _this.$apiHistoryTaking.getDataWaitingDoctor(dataParams)

        // กำลังเรียก
        if (_.get(dataParams, ['caller_id'])) {
          _this.rowsWaitingDoctor = _this.rowsWaitingDoctor.concat(data)
        } else {
          _this.rowsWaitingDoctor = data
        }
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // คิวพัก
    async fetchDataHold(params = {}) {
      const _this = this
      if (!_this.isReadySetting) return
      try {
        _this.setLoading(true)
        const defaultParams = {
          service_group_id: _this.service_group_id,
          service_ids: _this.service_ids,
          service_point_id: _this.service_point_id,
          queue_status_id: 3, // default คิวพัก
        }
        const dataParams = _this.$updateObject(defaultParams, params)
        const data = await _this.$apiHistoryTaking.getDataCalling(dataParams)

        if (_.get(dataParams, ['caller_id'])) {
          _this.rowsHold = _this.rowsCall.concat(data)
        } else {
          _this.rowsHold = data
        }
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // คิวทั้งหมด
    async fetchDataQueueList(newParams = {}) {
      const _this = this
      if (!_this.service_group_id) return
      try {
        _this.setLoading(true)
        const defaultParams = {
          service_group_id: _this.service_group_id,
        }
        const params = _this.$updateObject(defaultParams, newParams)
        const data = await _this.$apiHistoryTaking.getQueueList(params)
        if (_.get(params, ['queue_detail_id'])) {
          _this.rowsQueue = _this.rowsQueue.concat(data)
        } else {
          _this.rowsQueue = data
        }
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    /**
     * รายชื่อแพทย์ทั้งหมด
     */
    async fetchDataDoctors() {
      const _this = this
      try {
        const doctors = await _this.$apiHistoryTaking.getDoctors()
        _this.setDoctorItems(doctors)
      } catch (error) {
        _this.$errorHandler(error)
      }
    },
    /**
     * รายชื่อแพทย์ตามตารางเวลาออกตรวจ
     */
    async fetchDataDoctorSchedule() {
      const _this = this
      if (!_this.service_group_id) return
      try {
        await _this.$apiHistoryTaking.getDoctorSchedule({
          service_group_id: _this.service_group_id,
          service_ids: _this.service_ids,
        })
        // _this.$store.commit('taking/setDoctors', doctors)
      } catch (error) {
        _this.$errorHandler(error)
      }
    },
    /**
     * เช็คผล lab
     */
    async checkListReportLab() {
      const _this = this
      if (!_this.labVisitIdList.length) return
      try {
        const resp = await _this.$apiHistoryTaking.checkListReportLab({ listVisitId: _this.labVisitIdList })
        if (_.get(resp, 'resultStatus') === 'SUCCESS') {
          const listVisitLabResult = _.get(resp, 'listVisitLabResult', [])
          // ค้นหาเฉพาะรายการที่เสร็จสิ้น
          const visitSuccessList = listVisitLabResult.filter((item) => item.report_status === true)
          if (visitSuccessList.length) {
            const visitIdList = visitSuccessList.map((item) => item.visit_id) // ['id1', 'id2']
            const queueList = _this.filteredLabs.filter((row) => visitIdList.includes(row.visit_id))
            const queueIds = queueList.map((item) => item.queue_detail_id)
            await _this.$apiHistoryTaking.updateLabStatus({ queue_ids: queueIds })
            for (let i = 0; i < queueList.length; i++) {
              const queue = queueList[i]
              // แจ้งเตือน
              _this.$toastr.success(`${queue.fullname}`, `คิว Lab พร้อมเรียก #${queue.queue_no}`, {
                timeOut: 3000,
              })
            }
            await _this.fetchDataWaiting()
            await _this.fetchDataQueueList()
          }
        }
      } catch (error) {
        _this.$errorHandler(error)
      }
    },

    // คิวห้องแพทย์
    async fetchDataQueueDoctor(doctorId = '') {
      const _this = this
      if (!_this.service_group_id) return
      try {
        _this.setLoading(true)
        const data = await _this.$apiHistoryTaking.getDataQueueDoctor({
          service_group_id: _this.service_group_id,
          doctor_id: doctorId,
        })
        _this.rowsDoctor = data
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },

    // บันทึกการตั้งค่า
    async onSubmitFormSetting() {
      await this.fetchDataWaiting()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataCalling()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataHold()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataQueueList()
      this.$toastr.success(`การตั้งค่าถูกบันทึก`, ``, {
        timeOut: 3000,
        positionClass: 'toast-top-right',
        progressBar: true,
        closeButton: true,
      })
    },
    // ค้นหารายการคิวทั้งหมด
    async onSubmitFormSearch(params) {
      const _this = this
      if (!_this.service_group_id) return
      try {
        _this.setLoading(true)
        const data = await _this.$apiHistoryTaking.getQueueList({
          service_group_id: _this.service_group_id,
          queue_date: params.queue_date,
          doctor_id: params.doctor_id,
        })
        _this.rowsQueue = data
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },

    // เรียกคิวถัดไป
    onCallNext() {
      if (this.rowsWait.length) {
        this.onCallWait(this.rowsWait[0])
      } else {
        Swal.fire({
          icon: 'warning',
          title: 'Oops...',
          text: 'ไม่พบรายการคิวรอเรียก',
          confirmButtonText: 'ตกลง',
          allowOutsideClick: false,
        })
      }
    },
    // เรียกคิวรอเรียก
    onCallWait(queue) {
      const _this = this
      if (!this.isReadyCallWait) return
      const body = {
        queue_id: queue.queue_id,
        queue_detail_id: queue.queue_detail_id,
        service_point_id: _this.service_point_id,
      }
      Swal.fire({
        title: `เรียกคิว #${queue.queue_no} ?`,
        html: _this.replaceTemplateValue(queue),
        showCancelButton: true,
        confirmButtonText: '<i class="flaticon2-speaker text-white"></i> เรียกคิว',
        cancelButtonText: '<i class="flaticon2-cancel text-white"></i> ยกเลิก',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        allowOutsideClick: false,
        cancelButtonColor: '#f64e60',
        preConfirm: function () {
          return new Promise(function (resolve) {
            _this.$apiHistoryTaking
              .callWaiting(body)
              .then((res) => {
                resolve(res)
              })
              .catch((error) => _this.$errorHandler(error))
          })
        },
      }).then((result) => {
        if (result.value) {
          // sending socket event
          _this.$socket.emit(_this.$SOCKET_EVENTS.CALL_QUEUE, {
            data: result.value,
            action: 'call wait',
            message: 'เรียกคิวซักประวัติ',
            type: 'history-taking',
          })
          // แสดงคิวที่กำลังเรียก
          _this.currentQueueCall = _this.$updateObject(queue, {
            caller_id: result.value.caller_id,
          })
          // แจ้งเตือน
          _this.$toastr.success(`${queue.fullname}`, `เรียกคิว #${queue.queue_no}`, {
            timeOut: 3000,
          })
          // update data waiting
          _this.rowsWait = _this.rowsWait.filter((r) => r.queue_detail_id !== queue.queue_detail_id)
          _this.fetchDataCalling()
          _this.fetchDataQueueList()
        }
      })
    },
    // เรียกคิวกำลังเรียก
    onRecall(queue) {
      const _this = this
      if (!_this.isReadySetting) return
      const body = {
        caller_id: queue.caller_id,
        queue_id: queue.queue_id,
        queue_detail_id: queue.queue_detail_id,
        service_point_id: _this.service_point_id,
        service_ids: _this.service_ids,
      }
      Swal.fire({
        title: `เรียกคิว #${queue.queue_no} ?`,
        html: _this.replaceTemplateValue(queue),
        showCancelButton: true,
        confirmButtonText: '<i class="flaticon2-speaker text-white"></i> เรียกคิว',
        cancelButtonText: '<i class="flaticon2-cancel text-white"></i> ยกเลิก',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        allowOutsideClick: false,
        cancelButtonColor: '#f64e60',
        onBeforeOpen: () => {
          if (!_this.confirmCallDialog) {
            Swal.clickConfirm()
          }
        },
        preConfirm: function () {
          return new Promise(function (resolve) {
            _this.$apiHistoryTaking
              .recall(body)
              .then((res) => {
                resolve(res)
              })
              .catch((error) => _this.$errorHandler(error))
          })
        },
      }).then((result) => {
        if (result.value) {
          // sending socket event
          _this.$socket.emit(_this.$SOCKET_EVENTS.CALL_QUEUE, {
            data: result.value,
            action: 'recall',
            message: 'เรียกคิวซักประวัติ',
            type: 'history-taking',
          })
          // แสดงคิวที่กำลังเรียก
          _this.currentQueueCall = queue
          // แจ้งเตือน
          _this.$toastr.success(`${queue.fullname}`, `เรียกคิว #${queue.queue_no}`, {
            timeOut: 3000,
          })
          _this.rowsHold = _this.rowsHold.filter((r) => r.caller_id !== queue.caller_id)
          _this.rowsWaitingDoctor = _this.rowsHold.filter((r) => r.caller_id !== queue.caller_id)
          if (_this.currentTab === 'hold' || _this.currentTab === 'waiting-doctor') {
            _this.fetchDataCalling()
          }
          _this.fetchDataQueueList()
        }
      })
    },
    // พักคิว
    onHold(queue) {
      const _this = this
      if (!_this.isReadySetting) return
      const body = {
        caller_id: queue.caller_id,
        queue_id: queue.queue_id,
        queue_detail_id: queue.queue_detail_id,
        service_point_id: _this.service_point_id,
        service_ids: _this.service_ids,
      }
      Swal.fire({
        title: `พักคิว #${queue.queue_no} ?`,
        html: _this.replaceTemplateValue(queue),
        showCancelButton: true,
        confirmButtonText: '<i class="fas fa-hand-paper text-white"></i> พักคิว',
        cancelButtonText: '<i class="flaticon2-cancel text-white"></i> ยกเลิก',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        allowOutsideClick: false,
        cancelButtonColor: '#f64e60',
        onBeforeOpen: () => {
          if (!_this.confirmHoldDialog) {
            Swal.clickConfirm()
          }
        },
        preConfirm: function () {
          return new Promise(function (resolve) {
            _this.$apiHistoryTaking
              .hold(body)
              .then((res) => {
                resolve(res)
              })
              .catch((error) => _this.$errorHandler(error))
          })
        },
      }).then((result) => {
        if (result.value) {
          // sending socket event
          _this.$socket.emit(_this.$SOCKET_EVENTS.HOLD_QUEUE, {
            data: result.value,
            action: 'hold',
            message: 'พักคิวซักประวัติ',
            type: 'history-taking',
          })
          // แสดงคิวที่กำลังเรียก
          if (_this.currentQueueCall && _this.currentQueueCall.caller_id === queue.caller_id) {
            _this.currentQueueCall = null
          }
          // แจ้งเตือน
          _this.$toastr.warning(`${queue.fullname}`, `พักคิว #${queue.queue_no}`, {
            timeOut: 3000,
          })
          _this.rowsCall = _this.rowsCall.filter((r) => r.caller_id !== queue.caller_id)
          _this.fetchDataHold()
          _this.fetchDataQueueList()
        }
      })
    },
    // เสร็จสิ้นคิว
    async onEnd(queue) {
      const _this = this
      if (!_this.isReadySetting) return
      Swal.fire({
        title: '',
        text: 'กำลังค้นหารายชื่อแพทย์ที่ออกตรวจ...',
        timerProgressBar: true,
        showClass: {
          popup: 'swal2-noanimation',
        },
        onBeforeOpen: () => {
          Swal.showLoading()
          $(Swal.getTitle()).css('font-size', '2rem')
        },
      })
      const doctors = await _this.$apiHistoryTaking.getDoctorSchedule({
        service_group_id: _this.service_group_id,
        service_ids: _this.service_ids,
      })
      const inputOptions = {}
      doctors.map((d) => {
        inputOptions[d.doctor_id] = `${d.doctor_name}`
      })
      const body = {
        caller_id: queue.caller_id,
        queue_id: queue.queue_id,
        queue_detail_id: queue.queue_detail_id,
        service_point_id: _this.service_point_id,
        service_ids: _this.service_ids,
        service_group_id: queue.service_group_id,
      }
      Swal.fire({
        title: `เสร็จสิ้นคิว #${queue.queue_no} ?`,
        html: _this.replaceTemplateValue(queue),
        showCancelButton: true,
        confirmButtonText: '<i class="fas fa-user-check text-white"></i> เสร็จสิ้น',
        cancelButtonText: '<i class="flaticon2-cancel text-white"></i> ยกเลิก',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        allowOutsideClick: false,
        cancelButtonColor: '#f64e60',
        input: 'select',
        inputOptions: inputOptions,
        inputValue: _.get(queue, ['doctor_id'], null),
        customClass: {
          input: 'input-class',
        },
        inputPlaceholder: 'เลือกแพทย์',
        onBeforeOpen: () => {
          $(Swal.getInput()).addClass('form-control')
        },
        inputValidator: (value) => {
          if (!value) {
            return 'กรุณาเลือกแพทย์!'
          }
        },
        preConfirm: function (value) {
          return new Promise(function (resolve) {
            _this.$apiHistoryTaking
              .end(
                _this.$updateObject(body, {
                  doctor_id: value,
                })
              )
              .then((res) => {
                resolve(res)
              })
              .catch((error) => _this.$errorHandler(error))
          })
        },
      }).then((result) => {
        if (result.value) {
          // sending socket event
          _this.$socket.emit(_this.$SOCKET_EVENTS.END_QUEUE, {
            data: result.value,
            action: 'end',
            message: 'เสร็จสิ้นคิวซักประวัติ',
            type: 'history-taking',
          })
          _this.$socket.emit(
            _this.$SOCKET_EVENTS.REGISTER,
            _this.$updateObject(result.value, {
              message: 'ลงทะเบียนคิวห้องตรวจ',
              type: 'examination',
            })
          )
          // แสดงคิวที่กำลังเรียก
          if (_this.currentQueueCall && _this.currentQueueCall.caller_id === queue.caller_id) {
            _this.currentQueueCall = null
          }
          // แจ้งเตือน
          _this.$toastr.success(`${queue.fullname}`, `เสร็จสิ้นคิว #${queue.queue_no}`, {
            timeOut: 3000,
          })
          _this.rowsCall = _this.rowsCall.filter((r) => r.caller_id !== queue.caller_id)
          _this.rowsHold = _this.rowsHold.filter((r) => r.caller_id !== queue.caller_id)
          _this.fetchDataHold()
          _this.fetchDataQueueList()
        }
      })
    },
    // รอพบพทย์
    onWaitingDoctor(queue) {
      const _this = this
      if (!_this.isReadySetting) return
      const body = {
        caller_id: queue.caller_id,
        queue_id: queue.queue_id,
        queue_detail_id: queue.queue_detail_id,
        service_point_id: _this.service_point_id,
        service_ids: _this.service_ids,
      }
      Swal.fire({
        title: `รอพบแพทย์ #${queue.queue_no} ?`,
        html: _this.replaceTemplateValue(queue),
        showCancelButton: true,
        confirmButtonText: '<i class="fas fa-hand-paper text-white"></i> ยืนยัน',
        cancelButtonText: '<i class="flaticon2-cancel text-white"></i> ยกเลิก',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        allowOutsideClick: false,
        cancelButtonColor: '#f64e60',
        onBeforeOpen: () => {
          if (!_this.confirmHoldDialog) {
            Swal.clickConfirm()
          }
        },
        preConfirm: function () {
          return new Promise(function (resolve) {
            _this.$apiHistoryTaking
              .waitingDoctor(body)
              .then((res) => {
                resolve(res)
              })
              .catch((error) => _this.$errorHandler(error))
          })
        },
      }).then((result) => {
        if (result.value) {
          // sending socket event
          _this.$socket.emit(_this.$SOCKET_EVENTS.WAITING_DOCOTR_QUEUE, {
            data: result.value,
            action: 'waiting-doctor',
            message: 'คิวรอพบแพทย์',
            type: 'history-taking',
          })
          // แสดงคิวที่กำลังเรียก
          if (_this.currentQueueCall && _this.currentQueueCall.caller_id === queue.caller_id) {
            _this.currentQueueCall = null
          }
          // แจ้งเตือน
          _this.$toastr.warning(`${queue.fullname}`, `รอพบแพทย์ #${queue.queue_no}`, {
            timeOut: 3000,
          })
          _this.rowsCall = _this.rowsCall.filter((r) => r.caller_id !== queue.caller_id)
          _this.fetchDataWaitingDoctor()
          _this.fetchDataQueueList()
        }
      })
    },

    // ลบรายการคิว
    onDeleteQueue(queue) {
      const _this = this
      Swal.fire({
        icon: 'question',
        title: `ลบรายการคิว #${queue.queue_no} ?`,
        showCancelButton: true,
        confirmButtonText: '<i class="flaticon2-trash text-white"></i> ยืนยัน',
        cancelButtonText: '<i class="flaticon2-cancel text-white"></i> ยกเลิก',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        allowOutsideClick: false,
        cancelButtonColor: '#f64e60',
        preConfirm: function () {
          return new Promise(function (resolve) {
            _this.$apiHistoryTaking
              .deleteQueue(queue.queue_detail_id)
              .then((res) => {
                resolve(res)
              })
              .catch((error) => _this.$errorHandler(error))
          })
        },
      }).then((result) => {
        if (result.value) {
          Swal.fire({
            icon: 'success',
            title: result.value.message,
            text: '',
            timer: 3000,
            showLoaderOnConfirm: false,
            showConfirmButton: false,
          })
          _this.rowsWait = _this.rowsWait.filter((row) => row.quue_detail_id !== queue.quue_detail_id)
          _this.rowsCall = _this.rowsCall.filter((row) => row.quue_detail_id !== queue.quue_detail_id)
          _this.rowsHold = _this.rowsHold.filter((row) => row.quue_detail_id !== queue.quue_detail_id)
          _this.fetchDataQueueList()
          if (_this.currentQueueCall && _this.currentQueueCall.caller_id === queue.caller_id) {
            _this.currentQueueCall = null
          }
        }
      })
    },

    onReload(table) {
      if (table === 'waiting') {
        this.fetchDataWaiting()
      } else if (table === 'calling') {
        this.fetchDataCalling()
      } else if (table === 'hold') {
        this.fetchDataHold()
      } else if (table === 'queue-list') {
        this.fetchDataQueueList()
      } else if (table === 'doctor') {
        this.fetchDataQueueDoctor()
      } else if (table === 'waiting-doctor') {
        this.fetchDataWaitingDoctor()
      }
    },

    async onScreening(queue) {
      const _this = this
      if (!_this.isReadySetting) return
      Swal.fire({
        title: '',
        text: 'กำลังค้นหารายชื่อแพทย์ที่ออกตรวจ...',
        timerProgressBar: true,
        showClass: {
          popup: 'swal2-noanimation',
        },
        onBeforeOpen: () => {
          Swal.showLoading()
          $(Swal.getTitle()).css('font-size', '2rem')
        },
      })
      const doctors = await _this.$apiHistoryTaking.getDoctorSchedule({
        service_group_id: _this.service_group_id,
        service_ids: _this.service_ids,
      })
      const inputOptions = {}
      doctors.map((d) => {
        inputOptions[d.doctor_id] = `${d.doctor_name}`
      })

      let schedule = null
      let doctoreid = queue.doctor_eid
      if (Number(queue.doctor_id) === 0 && doctors.length > 0) {
        const doctorIds = doctors.map((d) => d.doctor_eid)
        doctoreid = doctorIds[Math.floor(Math.random() * doctorIds.length)]
        schedule = doctors.find((item) => item.doctor_eid === doctoreid)
      }

      Swal.fire({
        title: `คิว #${queue.queue_no} ?`,
        html: _this.replaceTemplateValue(queue),
        showCancelButton: true,
        confirmButtonText: 'ส่ง',
        cancelButtonText: 'ยกเลิก',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        allowOutsideClick: false,
        cancelButtonColor: '#f64e60',
        input: 'select',
        inputOptions: inputOptions,
        inputValue: _.get(queue, ['doctor_id'], ''),
        customClass: {
          input: 'input-class',
        },
        inputPlaceholder: 'เลือกแพทย์',
        onBeforeOpen: () => {
          $(Swal.getInput()).addClass('form-control')
        },
        inputValidator: (value) => {
          if (!value) {
            return 'กรุณาเลือกแพทย์!'
          }
        },
        preConfirm: function (value) {
          return new Promise(function (resolve) {
            resolve(value)
          })
        },
      }).then((result) => {
        if (result.value) {
          if (doctoreid && doctoreid !== 'nodoctor') {
            this.$socket.emit(_this.$SOCKET_EVENTS.SEND_DATA, {
              action: 'screen nurse',
              receiveEvent: 'test',
              hn: queue.hn,
              caller_id: queue.caller_id,
              queue_id: queue.queue_detail_id,
              doctor_eid: result.value,
              user_id: this.user.ref_user_id,
              service_point_id: _.get(schedule, 'service_point_id', ''),
            })
            Swal.fire({
              icon: 'success',
              title: 'ส่งข้อมูลไปยังระบบ iMed แล้ว!',
              text: '',
              timer: 1500,
              showConfirmButton: false,
            })
          } else {
            Swal.fire({
              icon: 'warning',
              title: 'ไม่พบข้อมูลแพทย์',
              text: '',
              confirmButtonText: 'ตกลง',
            })
          }
        }
      })
    },

    replaceTemplateValue(queue) {
      return template.profileHistory
        .replace('{img}', picture)
        .replace('{hn}', queue.hn)
        .replace('{fullname}', `${queue.fullname}`)
        .replace('{service_group_name}', queue.service_group_name)
        .replace('{service_point_name}', this.servicePointName)
        .replace('{draw_blood_description}', queue.draw_blood_description)
        .replace('{xray_description}', queue.xray_description)
    },

    // socket events
    async socketOnRegisterQueue(data) {
      if (_.get(data, ['type'], '') === 'history-taking') {
        this.$toastr.success(
          `ชื่อ-นามสกุล: ${data.fullname}<br />งานบริการ: ${data.service_group_name}<br /><span class="text-muted">${data.service_name}</span>`,
          `คิวลงทะเบียนใหม่! #${data.queue_no}`,
          {
            timeOut: 3000,
            positionClass: 'toast-top-right',
            progressBar: true,
            closeButton: true,
          }
        )
        await Promise.all([this.fetchDataWaiting(), this.fetchDataQueueList(), this.checkListReportLab()])
      }
    },
    // event เรียกคิว
    async socketOnCallQueue(data) {
      const isMatchEvent = this.isMatchEvent(data, true) // check service point
      if (isMatchEvent) {
        if (data.action === 'call wait') {
          this.rowsWait = this.rowsWait.filter((row) => row.queue_detail_id !== data.data.queue_detail_id)
          this.rowsQueue = this.rowsQueue.filter((row) => row.queue_detail_id !== data.data.queue_detail_id)
          await this.fetchDataCalling()
          await this.fetchDataQueueList()
        }
        if (data.action === 'recall') {
          this.rowsCall = this.rowsCall.filter((row) => row.caller_id !== data.data.caller_id)
          this.rowsHold = this.rowsHold.filter((row) => row.caller_id !== data.data.caller_id)
          this.rowsQueue = this.rowsQueue.filter((row) => row.queue_detail_id !== data.data.queue_detail_id)
          await this.fetchDataCalling()
          await this.fetchDataQueueList()
          await this.fetchDataWaitingDoctor()
        }
        this.$toastr.success(`${data.data.fullname}`, `เรียกคิว #${data.data.queue_no}`, {
          timeOut: 3000,
        })
      } else if (_.get(data, ['type'], '') === 'examination') {
        if (data.action === 'recall') {
          await this.fetchDataQueueDoctor()
        }
      }
    },
    // event พักคิว
    async socketOnHoldQueue(data) {
      const _this = this
      const isMatchEvent = _this.isMatchEvent(data, false) // check service point
      if (isMatchEvent) {
        if (data.action === 'hold') {
          if (_this.currentQueueCall && _this.currentQueueCall.caller_id === parseInt(data.data.caller_id)) {
            _this.currentQueueCall = null
          }
          this.$toastr.warning(`${data.data.fullname}`, `พักคิว #${data.data.queue_no}`, {
            timeOut: 3000,
          })
          this.rowsCall = this.rowsCall.filter((row) => row.caller_id !== data.data.caller_id)
          this.rowsQueue = this.rowsQueue.filter((row) => row.queue_detail_id !== data.data.queue_detail_id)
          await this.fetchDataHold()
          await this.fetchDataQueueList()
        }
      }
    },
    // event เสร็จสิ้นคิว
    async socketOnEndQueue(data) {
      const _this = this
      const isMatchEvent = this.isMatchEvent(data, true) // check service point
      if (isMatchEvent) {
        if (data.action === 'end') {
          if (_this.currentQueueCall && _this.currentQueueCall.caller_id === parseInt(data.data.caller_id)) {
            _this.currentQueueCall = null
          }
          this.$toastr.success(`${data.data.fullname}`, `เสร็จสิ้นคิว #${data.data.queue_no}`, {
            timeOut: 3000,
          })
          this.rowsWaitingDoctor = this.rowsWaitingDoctor.filter((row) => row.caller_id !== data.data.caller_id)
          this.rowsCall = this.rowsCall.filter((row) => row.caller_id !== data.data.caller_id)
          this.rowsHold = this.rowsHold.filter((row) => row.caller_id !== data.data.caller_id)
          this.rowsQueue = this.rowsQueue.filter((row) => row.queue_detail_id !== data.data.queue_detail_id)
          await this.fetchDataQueueList()
        }
      }
    },
    async socketOnScreenFinish(data) {
      const _this = this
      _this.rowsCall = _this.rowsCall.filter((r) => r.caller_id !== parseInt(data.caller_id))
      _this.rowsHold = _this.rowsHold.filter((r) => r.caller_id !== parseInt(data.caller_id))
      //   // แสดงคิวที่กำลังเรียก
      if (_this.currentQueueCall && _this.currentQueueCall.caller_id === parseInt(data.caller_id)) {
        _this.currentQueueCall = null
      }
      _this.$toastr.success(`${data.prename}${data.firstname} ${data.lastname}`, `เสร็จสิ้นคิว #${data.queue_no}`, {
        timeOut: 3000,
      })
      await _this.fetchDataHold()
      await _this.fetchDataQueueList()
    },
    // แสดงจอทีวี
    socketOnAudioPlaying(data) {
      const _this = this
      const callerId = _.get(_this.currentQueueCall, 'caller_id')
      if (callerId !== Number(data.caller_id)) {
        _this.$toastr.success(`${data.patient_name}`, `เรียกคิว #${data.queue_no}`, {
          timeOut: 3000,
        })
      } else if (!callerId) {
        _this.$toastr.success(`${data.patient_name}`, `เรียกคิว #${data.queue_no}`, {
          timeOut: 3000,
        })
      }
    },
    // ยกเลิกคิว
    socketOnCancelQueue(data) {
      this.$toastr.warning(`${data.fullname}`, `คิวถูกยกเลิก #${data.queue_no}`, {
        timeOut: 3000,
      })
      this.fetchDataQueueList()
    },
    // รอพบแพทย์
    socketOnWaitingDoctor(data) {
      const isMatchEvent = this.isMatchEvent(data, false) // check service point
      if (isMatchEvent) {
        this.$toastr.warning(`${data.data.fullname}`, `คิวรอพบแพทย์ #${data.data.queue_no}`, {
          timeOut: 3000,
        })
        this.rowsCall = this.rowsCall.filter((row) => row.caller_id !== data.data.caller_id)
        this.rowsHold = this.rowsHold.filter((row) => row.caller_id !== data.data.caller_id)
        this.fetchDataQueueList()
        this.fetchDataWaitingDoctor()
      }
    },
    /**
     * พิมพ์บัตรคิว
     * @param {Number} queueId queue_detail_id
     */
    onPrintTicket(queueId) {
      const _this = this
      _this.$socket.emit(_this.$SOCKET_EVENTS.PRINTING_QUEUE, {
        queueId: queueId,
        message: 'พิมพ์บัตรคิว',
      })
      const windowSize = 'width=400, height=400'
      window.open(`${_this.$env.PRINT_BASE_URL}?id=${queueId}`, 'myPrint', windowSize)
    },
    isMatchEvent(data, checkServicePoint = true) {
      const currentServicePointId = this.service_point_id
      const isMatchEvent = _.get(data, ['type'], '') === this.eventType
      const eventServicePointId = _.get(data, 'data.service_point_id', '')
      if (checkServicePoint) {
        return currentServicePointId === eventServicePointId && isMatchEvent
      }
      return isMatchEvent
    },
  },
}

export default mixins

import { mapActions, mapGetters, mapState } from 'vuex'
import _ from 'lodash'
import isEmpty from 'is-empty'
import { KIOSK_MODULE_NAME, GET_SECTIONS, GET_CURRENT_SECTION_ID, GET_CURRENT_SECTION } from '@/core/types/kiosk'
import KioskSawatdee from '@/components/Kiosk/KioskSawatdee'
import KioskSections from '@/components/Kiosk/KioskSections'
import SectionPersonalCard from '@/components/Kiosk/Section/SectionPersonalCard'
import SectionQRCode from '@/components/Kiosk/Section/SectionQRCode'
import SectionPersonalRFID from '@/components/Kiosk/Section/SectionPersonalRFID'
import KioskNumpad from '@/components/Kiosk/KioskNumpad'
import Wizard from '@/components/Kiosk/Wizard/Wizard'
import WizardNav from '@/components/Kiosk/Wizard/WizardNav'
import WizardStepNav from '@/components/Kiosk/Wizard/WizardStepNav'
import WizardBody from '@/components/Kiosk/Wizard/WizardBody'
import SectionTitle from '@/components/Kiosk/Section/SectionTitle'
import KioskPatientInfo from '@/components/Kiosk/KioskPatientInfo'
import SectionServiceGroup from '@/components/Kiosk/Section/SectionServiceGroup'
import LdsLoading from '@/components/Base/Spinner/LdsLoading'

const mixins = {
  components: {
    KioskSawatdee,
    KioskSections,
    KioskNumpad,
    SectionPersonalCard,
    SectionQRCode,
    SectionPersonalRFID,
    Wizard,
    WizardNav,
    WizardStepNav,
    WizardBody,
    SectionTitle,
    KioskPatientInfo,
    SectionServiceGroup,
    LdsLoading,
  },
  directives: {
    focus: {
      // directive definition
      inserted: function (el) {
        el.focus()
      },
    },
  },
  beforeMount() {
    const EVENTS = this.$SOCKET_EVENTS
    const sockets = this.sockets.listener
    sockets.subscribe(EVENTS.CARD_INSERTED, this.handlerCardInsert)
    sockets.subscribe(EVENTS.CARD_REMOVED, this.handlerCardRemove)
    sockets.subscribe(EVENTS.READING_START, this.handlerCardReading)
    sockets.subscribe(EVENTS.READING_COMPLETE, this.handlerCardReadingComplete)
    sockets.subscribe(EVENTS.READING_FAIL, this.handlerCardReadingFail)
    sockets.subscribe(EVENTS.DEVICE_DISCONNECTED, this.handlerCardDisconnect)
    sockets.subscribe('send client ip', this.handlerSendClientIP)
  },
  beforeDestroy() {
    const EVENTS = this.$SOCKET_EVENTS
    const sockets = this.sockets.listener
    sockets.unsubscribe(EVENTS.CARD_INSERTED)
    sockets.unsubscribe(EVENTS.CARD_REMOVED)
    sockets.unsubscribe(EVENTS.READING_START)
    sockets.unsubscribe(EVENTS.READING_COMPLETE)
    sockets.unsubscribe(EVENTS.READING_FAIL)
    sockets.unsubscribe(EVENTS.DEVICE_DISCONNECTED)
    sockets.unsubscribe('send client ip')
  },
  computed: {
    ...mapGetters({
      // ตัวเลือกการออกบัตรคิว
      sections: `${KIOSK_MODULE_NAME}/${GET_SECTIONS}`,
      sectionId: `${KIOSK_MODULE_NAME}/${GET_CURRENT_SECTION_ID}`,
      currentSection: `${KIOSK_MODULE_NAME}/${GET_CURRENT_SECTION}`,
    }),
    ...mapState({
      loading: (state) => _.get(state.kiosk, 'loading', false),
      section: (state) => _.get(state.kiosk, 'section', ''),
      search: (state) => _.get(state.kiosk, 'search', ''),
      showModal: (state) => _.get(state.kiosk, 'showModal', false),
      clientIP: (state) => _.get(state.kiosk, 'ipAddress', '0.0.0.0'),
      serviceGroups: (state) => _.get(state.kiosk, 'service_groups', []),
      patient: (state) => _.get(state.kiosk, 'patient', null),
      personalInfo: (state) => _.get(state.kiosk, 'personal_info', null),
      patientRight: (state) => _.get(state.kiosk, 'patient_right', null),
      appoints: (state) => _.get(state.kiosk, 'appoints', []),
      service_group_id: (state) => _.get(state.kiosk, 'service_group_id', ''),
      service_id: (state) => _.get(state.kiosk, 'service_id', ''),
      doctor_id: (state) => _.get(state.kiosk, 'doctor_id', ''),
      doctors: (state) => _.get(state.kiosk, 'doctors', []),
      appoint_id: (state) => _.get(state.kiosk, 'appoint_id', []),
      draw_blood: (state) => _.get(state.kiosk, 'draw_blood', false),
      xray: (state) => _.get(state.kiosk, 'xray', false),
    }),
    // ทำรายการด้วย
    sectionLabel() {
      const labels = ['ทำรายการด้วย']
      labels.push(_.get(this.currentSection, 'label', ''))
      return labels.join(' ')
    },
    // แสดงการทำรายการด้วยบัตร
    isShowSectionCID() {
      return this.sectionId === 'section-cid'
    },
    // แสดงตัวเลือกการทำรายการ
    isShowSections() {
      return !this.isShowSectionCID && !this.isShowSectionQRCode && !this.isShowSectionPersonal
    },
    // แสดงปุ่มยกเลิกรายการ
    isShowButtonCancel() {
      return !isEmpty(this.section) && this.sectionId !== 'section-hn'
    },
    // แสดงการทำรายการด้วยคิวอาร์โค้ด
    isShowSectionQRCode() {
      return this.sectionId === 'section-qrcode'
    },
    // แสดงการทำรายการด้วยบัตรพนักงาน
    isShowSectionPersonal() {
      return this.sectionId === 'section-personal'
    },
    // ตรวจสอบว่ามีข้อมูลผู้ป่วยหรือไม่
    isPatient() {
      return !isEmpty(this.patient)
    },
    // กลุ่มงานบริการที่กำลังเลือก
    currentServiceGroup() {
      return this.serviceGroups.find((group) => group.service_group_id === this.service_group_id)
    },
    // ชื่อกลุ่มงานบริการที่เลือก
    serviceGroupName() {
      return _.get(this.currentServiceGroup, 'service_group_name', '-')
    },
    // รายการงานบริการตามกลุ่มบริการที่เลือก
    services() {
      return _.get(this.currentServiceGroup, 'services', [])
    },
    // งานบริการที่เลือก
    serviceSelcted() {
      return this.services.find((service) => Number(service.service_id) === Number(this.service_id))
    },
    // ชื่องานบริการที่เลือก
    serviceName() {
      return _.get(this.serviceSelcted, 'service_name', '-')
    },
    // แพทย์ที่เลือก
    doctorSelected() {
      return this.doctors.find((doctor) => Number(doctor.doctor_id) === Number(this.doctor_id))
    },
    // ชื่อแพทย์ที่เลือก
    doctorName() {
      return _.get(this.doctorSelected, 'doctor_name', '-')
    },
  },
  methods: {
    ...mapActions({
      setLoading: 'kiosk/setLoading',
      setSection: 'kiosk/setSection',
      setSearchValue: 'kiosk/setSearchValue',
      setStateValue: 'kiosk/setStateValue',
      setIpAddress: 'kiosk/setIpAddress',
      toggleModal: 'kiosk/toggleModal',
      setPersonalInfo: 'kiosk/setPersonalInfo',
      setServiceGroupItems: 'kiosk/setServiceGroupItems',
      setPatient: 'kiosk/setPatient',
      setPatientRight: 'kiosk/setPatientRight',
      setServiceGroupId: 'kiosk/setServiceGroupId',
      setServiceId: 'kiosk/setServiceId',
      setDoctorId: 'kiosk/setDoctorId',
      setDoctorItems: 'kiosk/setDoctorItems',
      setAppointId: 'kiosk/setAppointId',
      setDrawBlood: 'kiosk/setDrawBlood',
      setXray: 'kiosk/setXray',
    }),
    // ทำรายการด้วย
    onSelectSection(section) {
      this.setSection(section)
      // ถ้าทำรายการด้วย hn
      if (section.id === 'section-hn') {
        this.toggleModal()
        setTimeout(() => {
          this.$refs.input_search.focus()
        }, 300)
      }
    },
    // click numpad event
    onClickNumpad(number) {
      let search = String(this.search)
      if (String(search).length < 13) {
        search = search + number.toString()
        this.setSearchValue(search)
        this.$refs.input_search.focus()
      }
    },
    // click clear numpad
    onClearNumpad() {
      this.setSearchValue('')
      this.$refs.input_search.focus()
    },
    // click delete numpad
    onDeleteNumpad() {
      if (this.search) {
        this.setSearchValue(this.search.substr(0, this.search.length - 1))
      }
      this.$refs.input_search.focus()
    },
    // close modal handler
    onCloseModal() {
      this.toggleModal()
      this.setSearchValue('')
      this.setSection('')
    },
    // ยกเลิกรายการ
    onCancel() {
      this.cidLoding = false
      this.cidMessage = 'เสียบบัตรประชาชนของคุณ...'
      this.setLoading(false)
      this.setSearchValue('')
      this.setSection('')
      this.setPersonalInfo(null)
      this.setPatient(null)
    },

    /**
     * Socket io events
     */
    setCIDLoaing(loading = false) {
      this.cidLoding = loading
    },
    setCIDMessage(message = '') {
      this.cidMessage = message
    },
    // เสียบบัตร ปชช
    handlerCardInsert(resp) {
      if (this.isMatchIPAddress(resp) && this.isShowSectionCID) {
        this.setCIDLoaing(true)
        this.setCIDMessage('กรุณารอสักครู่...')
        this.setPersonalInfo(null)
      }
    },
    // ถอดบัตร ปชช
    handlerCardRemove(resp) {
      if (this.isMatchIPAddress(resp) && this.isShowSectionCID) {
        this.onCancel()
        this.setCIDLoaing(false)
        this.setCIDMessage('เสียบบัตรประชาชนของคุณ...')
        this.setPersonalInfo(null)
      }
    },
    // กำลังอ่านบัตร
    handlerCardReading(resp) {
      if (this.isMatchIPAddress(resp) && this.isShowSectionCID) {
        this.setCIDLoaing(true)
        this.setCIDMessage('ระบบกำลังอ่านข้อมูลบัตร...')
      }
    },
    // อ่านบัตรสำเร็จ
    handlerCardReadingComplete(resp) {
      if (this.isMatchIPAddress(resp) && this.isShowSectionCID) {
        this.setCIDLoaing(false)
        this.setCIDMessage('เสียบบัตรประชาชนของคุณ...')
        this.setPersonalInfo(resp.data)
        this.setSearchValue(resp.data.citizen_id)
        this.onSubmitModalForm()
      }
    },
    // อ่านบัตรเกิดข้อผิดพลาด
    handlerCardReadingFail(resp) {
      if (this.isMatchIPAddress(resp) && this.isShowSectionCID) {
        this.onCancel()
        this.setCIDLoaing(false)
        this.setCIDMessage('เกิดข้อผิดพลาดในการอ่านข้อมูลบัตร กรุณาลองใหม่อีกครั้ง!')
        setTimeout(() => {
          this.setCIDMessage('เสียบบัตรประชาชนของคุณ...')
        }, 3000)
      }
    },
    // สถานะเครื่องอ่านบัตร
    handlerCardDisconnect(resp) {
      if (this.isMatchIPAddress(resp) && this.isShowSectionCID) {
        this.setCIDLoaing(false)
        this.setCIDMessage('เสียบบัตรประชาชนของคุณ...')
      }
    },
    handlerSendClientIP(resp) {
      const _this = this
      if (_this.ipRequestKey === _.get(resp, 'ipRequestKey', '')) {
        _this.setIpAddress(_.get(resp, 'ipAddress', ''))
      }
    },
    // ตรวจสอบว่าไอพีตรงกันหรือไม่?
    isMatchIPAddress(resp) {
      const socketIP = _.get(resp, 'ipAddress', '0.0.0.0') // ไอพี client ที่ส่งข้อมูล
      const clientIP = this.clientIP // ไอพีตู้ kiosk
      return clientIP === socketIP
    },
    // พิมพ์บัตรคิว
    onPrintTicket(queueId) {
      const _this = this
      _this.$socket.emit(_this.$SOCKET_EVENTS.PRINTING_QUEUE, {
        queueId: queueId,
        message: 'พิมพ์บัตรคิว',
      })
      const windowSize = 'width=300, height=300'
      window.open(`${_this.$env.PRINT_BASE_URL}?id=${queueId}`, 'myPrint', windowSize)
    },
  },
}
export default mixins

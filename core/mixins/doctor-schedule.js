import { mapState } from 'vuex'
import _ from 'lodash'
import XLSX from 'xlsx'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import timeGridPlugin from '@fullcalendar/timegrid'
import listPlugin from '@fullcalendar/list'
import moment from 'moment'
import * as utils from '@/utils'
import {
  SCHEDULE_MODULE_NAME,
  SET_DATE_DISABLED,
  SET_HOLIDAY_ITEMS,
  SET_SERVICE_ITEMS,
  SET_EVENTS_ITEMS,
  SET_SERVICE_POINT_ITEMS,
} from '@/core/types/schedule'
moment.locale('th')

const mixins = {
  data: function () {
    return {
      loading: false,
      submitted: false,
      maximized: false,
      showForm: false,
      calendarPlugins: [dayGridPlugin, interactionPlugin, timeGridPlugin, listPlugin],
      calendarOptions: {
        locale: 'th',
        initialView: 'dayGridMonth',
        events: [],
        header: {
          left: 'prev,next today, exportButton',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek,reloadButton',
        },
        buttonText: {
          today: 'วันนี้',
          month: 'เดือน',
          week: 'สัปดาห์',
          list: 'กำหนดการ',
          day: 'วัน',
        },
        customButtons: {
          exportButton: {
            text: 'Export Excel',
            click: this.exportToExcel,
          },
          reloadButton: {
            text: 'Reload',
            click: this.reloadEvents,
          },
        },
        firstDay: 1,
        selectable: true,
        allDayText: 'ตลอดวัน',
        slotLabelFormat: {
          hour: 'numeric',
          minute: '2-digit',
          omitZeroMinute: false,
          meridiem: 'long',
        },
        minTime: '06:00:00',
        maxTime: '24:00:00',
        // aspectRatio: 2,
        timeFormat: 'HH:mm',
        eventTimeFormat: {
          hour: 'numeric',
          minute: '2-digit',
          meridiem: false,
        },
        displayEventEnd: true,
        titleFormat: {
          // will produce something like "Tuesday, September 18, 2018"
          month: 'short',
          year: 'numeric',
          day: 'numeric',
          weekday: 'short',
        },
        columnHeaderFormat: {
          weekday: 'short',
        },
        hiddenDays: [0, 6],
        editable: true,
        droppable: true,
        dragRevertDuration: 0,
        eventLimit: true, // allow "more" link when too many events
        navLinks: true,
        businessHours: true,
        nowIndicator: true,
        views: {
          dayGridMonth: {
            eventLimit: 4,
          },
        },
        eventLimitText: 'เพิ่มเติม',
        slotDuration: '00:30:00',
        eventRender: this.eventRender,
      },
      // รายชื่อกลุ่มงานบริการ
      serviceGroups: [],
      // รายชื่อแพทย์
      doctors: [],

      // วันที่
      dpkOptions: {
        autoclose: false,
        language: 'th',
        todayBtn: true,
        todayHighlight: true,
        multidate: true,
        format: 'yyyy-mm-dd',
        datesDisabled: [],
      },

      // copy data
      copyFilterKey: '',
      eventsCopyList: [],
      events_copy: [],
      formCopy: {
        items: [],
      },

      // table fields
      fields: [
        { key: 'index', label: '#', _style: 'width:35px', filter: false, sorter: false },
        { key: 'service_group_name', label: 'กลุ่มงานบริการ' },
        { key: 'service_name', label: 'ชื่องานบริการ' },
        { key: 'service_point_description', label: 'จุดบริการ' },
        { key: 'doctor_fullname', label: 'แพทย์' },
        { key: 'schdule_time', label: 'เวลา' },
        { key: 'queue_total_qty', label: 'จำนวนผู้ป่วยที่รับ' },
        { key: 'queue_assign_qty', label: 'จำนวนที่ระบุแพทย์ได้' },
        {
          key: 'actions',
          label: 'ดำเนินการ',
          filter: false,
          sorter: false,
        },
      ],
    }
  },
  filters: {
    scheduleTime: function (item) {
      if (!item) return ''
      const date = moment(item.schedule_date).format('YYYY-MM-DD')
      const stime = moment(`${date} ${item.start_time}`).format('HH:mm')
      const etime = moment(`${date} ${item.end_time}`).format('HH:mm')
      return `${stime}-${etime} น.`
    },
  },
  computed: {
    ...mapState({
      filterKey: (state) => state.schedule.filterKey,
      service_group_filter_key: (state) => state.schedule.service_group_filter_key,
      doctor_filter_key: (state) => state.schedule.doctor_filter_key,
      filter_days: (state) => state.schedule.filter_days,
      holidayOnly: (state) => state.schedule.holiday_only,
      holidays: (state) => state.schedule.holidays,
      datesDisabled: (state) => state.schedule.datesDisabled,
      services: (state) => state.schedule.services,
      events: (state) => state.schedule.events,
      service_points: (state) => state.schedule.service_points,
    }),
    // รายชื่อกลุ่มงานบริการ
    mapServiceGroups() {
      return utils.mapDataOptions(this.serviceGroups, 'service_group_id', 'service_group_name')
    },
    // แพทย์
    mapDoctors() {
      return utils.mapDataOptions(this.doctors, 'doctor_fullname', 'doctor_fullname')
    },
    // จุดบริการ
    mapServicePoints() {
      return this.service_points
        .filter((row) => row.service_point_status === 1)
        .map((row) => {
          return { id: row.service_point_id, text: row.service_point_description }
        })
    },
    // วันที่ 1 มกราคม ของปีปัจจุบัน
    startDateOfYear() {
      return moment().startOf('years').format('MM/DD/YYYY')
    },
    // วันที่ 31 ธันวาคม ของปีปัจจุบัน
    endDateOfYear() {
      return moment().endOf('years').format('MM/DD/YYYY')
    },
    /**
     * รายการวันหยุด
     */
    holidayEvents() {
      return this.holidays.map((holiday) => {
        const d = String(holiday.HolidayDate).split('/')
        return {
          title: holiday.HolidayName,
          start: moment(`${d[2]}-${d[1]}-${d[0]} 00:00`).format(),
          end: moment(`${d[2]}-${d[1]}-${d[0]} 23:59`).format(),
          allDay: true,
          editable: false,
          className: 'fc-event-light fc-event-solid-danger',
        }
      })
    },
    /**
     * รายการทั้งหมด
     */
    calendarEvents() {
      const filterKey = this.filterKey && this.filterKey.toLowerCase()
      const filterDoctor = this.doctor_filter_key && this.doctor_filter_key.toLowerCase()
      const filterServiceGroupId = this.service_group_filter_key && this.service_group_filter_key.toLowerCase()
      const filterDays = this.filter_days
      let events = this.holidayEvents.concat(this.events)
      if (this.holidayOnly) {
        events = _.orderBy(this.holidayEvents, ['start'], ['asc'])
      }
      if (filterKey) {
        events = events.filter(function (row) {
          return Object.keys(row).some(function (key) {
            return String(row[key]).toLowerCase().includes(filterKey)
          })
        })
      }
      if (filterDays.length) {
        events = events.filter(function (row) {
          return filterDays.includes(moment(row.start).format('dd'))
        })
      }
      if (filterDoctor) {
        events = events.filter(function (row) {
          const re = new RegExp(row.title)
          return re.test(filterDoctor)
        })
      }
      if (filterServiceGroupId) {
        events = events.filter(function (row) {
          return row.extendedProps && row.extendedProps.service_group_id === parseInt(filterServiceGroupId)
        })
        events = events ? this.holidayEvents.concat(events) : this.holidayEvents
      }
      return _.orderBy(events, ['start'], ['asc'])
    },
    // select2 options
    doctorOptions() {
      const opts = utils.mapDataOptions(this.doctors, 'doctor_id', 'doctor_fullname')
      return {
        placeholder: 'เลือกรายการ',
        data: [
          {
            id: '',
            text: 'เลือกรายการ',
          },
          ...opts,
        ],
        language: 'th',
        allowClear: true,
        width: '100%',
      }
    },
    // ตัวเลือกจุดบริการ
    servicePointOpts() {
      const opts = this.mapServicePoints
      return {
        placeholder: 'เลือกจุดบริการ',
        data: [
          {
            id: '',
            text: 'เลือกรายการ',
          },
          ...opts,
        ],
        language: 'th',
        allowClear: true,
        width: '100%',
      }
    },
    // ตัวเลือกกลุ่มงานบริการ
    serviceGroupOptions() {
      const opts = this.mapServiceGroups
      return {
        placeholder: 'งานบริการทั้งหมด',
        data: [
          {
            id: '',
            text: 'เลือกรายการ',
          },
          ...opts,
        ],
        language: 'th',
        allowClear: true,
        width: '100%',
      }
    },
    // ตัวเลือกงานบริการ
    serviceOptions() {
      const services = this.services.filter(
        (row) => Number(row.service_group_id) === Number(this.form.service_group_id)
      )
      const opts = utils.mapDataOptions(services, 'service_id', 'service_name')
      return {
        placeholder: 'เลือกงานบริการ',
        data: [
          {
            id: '',
            text: 'เลือกรายการ',
          },
          ...opts,
        ],
        language: 'th',
        allowClear: true,
        width: '100%',
      }
    },
    serviceListOfGroup() {
      const services = this.services.filter(
        (row) => Number(row.service_group_id) === Number(this.form.service_group_id)
      )
      return services
    },

    /* Datepicker options */
    datepickerOptions() {
      const _this = this
      return this.$updateObject(_this.dpkOptions, {
        beforeShowDay: function (date) {
          const d = moment(date).format('YYYY-MM-DD')
          if (_this.datesDisabled.includes(d)) {
            const data = _this.holidayEvents.find((row) => moment(row.start).format('YYYY-MM-DD') === d)
            if (data) {
              return {
                tooltip: data.title,
              }
            }
          }
        },
      })
    },
  },
  watch: {
    // วันหยุด
    holidays: function (holidays) {
      const DatesDisabled = holidays.map((r) => {
        const d = String(r.HolidayDate).split('/')
        return `${d[2]}-${d[1]}-${d[0]}`
      })
      this.$store.commit(`${SCHEDULE_MODULE_NAME}/${SET_DATE_DISABLED}`, DatesDisabled)
    },
    // วันหยุด
    datesDisabled: function (datesDisabled) {
      this.$nextTick(function () {
        $('#datepicker-inline').datepicker('setDatesDisabled', datesDisabled)
        $('#schedule_date').datepicker('setDatesDisabled', datesDisabled)
      })
    },
    /* form: function () {
      this.$nextTick(function () {
        $('#datepicker-inline').datepicker('setDatesDisabled', this.datesDisabled)
        $('#schedule_date').datepicker('setDatesDisabled', this.datesDisabled)
      })
    }, */
  },
  mounted() {
    this.fetchData()
  },
  methods: {
    /* calendar events render */
    eventRender(info) {
      const element = $(info.el)

      if (info.event.extendedProps && info.event.extendedProps.description) {
        const props = info.event.extendedProps
        if (element.hasClass('fc-day-grid-event')) {
          const date = moment(props.schedule_date).format('YYYY-MM-DD')
          const stime = moment(`${date} ${props.start_time}`).format('HH:mm')
          const etime = moment(`${date} ${props.end_time}`).format('HH:mm')
          element.data(
            'content',
            `
              <p class="no-margin"><strong>กลุ่มงานบริการ: </strong>${props.service_group_name}</p>
              <p class="no-margin"><strong>จุดบริการ: </strong>${props.service_point_description}</p>
              <p class="no-margin"><strong>แพทย์: </strong>${props.doctor_fullname}</p>
              <p class="no-margin"><strong>เวลา: </strong>${stime}-${etime} น.</p>
              <p class="no-margin"><strong>จำนวนผู้ป่วยที่รับ: </strong>${props.queue_total_qty}</p>
              <p class="no-margin"><strong>จำนวนคิวที่ระบุแพทย์ได้: </strong>${props.queue_assign_qty}</p>
              `
          )
          element.data('placement', 'top')
          element.data('html', true)
          KTApp.initPopover(element)
        } else if (element.hasClass('fc-time-grid-event')) {
          element
            .find('.fc-title')
            .append('<div class="fc-description">' + info.event.extendedProps.description + '</div>')
        } else if (element.find('.fc-list-item-title').lenght !== 0) {
          element
            .find('.fc-list-item-title')
            .append('<div class="fc-description">' + info.event.extendedProps.description + '</div>')
        }
      }
    },
    // card fullscreen
    toggleFullscreen() {
      this.maximized = !this.maximized
    },
    // datepicker filter
    onChangeDateInline(e) {
      const calendarApi = this.$refs.fullCalendar.getApi()
      calendarApi.changeView('timeGridDay', e.format())
    },
    // datepicker filter
    onChangeMonthInline(e) {
      const calendarApi = this.$refs.fullCalendar.getApi()
      calendarApi.changeView('dayGridMonth', e.date)
      $('#datepicker-inline').datepicker('update', e.date)
    },
    async fetchData() {
      await this.fetchDataHoliday()
      await this.fetchDataServices()
      await this.fetchDataServicePoints()
    },
    /**
     * วันหยุดประจำปี
     */
    async fetchDataHoliday() {
      const _this = this
      try {
        _this.setLoading(true)

        let holidays = []
        holidays = holidays.concat(_this.holidays)

        // api
        const params = {
          action: 'GetHolidayMonthly',
          sdate: _this.startDateOfYear, // วันที่เริ่มต้น
          edate: _this.endDateOfYear, // วันที่สิ้นสุด
        }
        const data = await _this.$apiSchedule.getHolidays(params)
        const Holidays1 = _this.convertXmlObject(data)
        holidays = holidays.concat(Holidays1)

        // วันหยุดเฉพาะเขตจังหวัด
        const params2 = _.merge(params, {
          province: 'นนทบุรี',
        })
        const dataProvince = await _this.$apiSchedule.getHolidays(params2)
        const Holidays2 = _this.convertXmlObject(dataProvince)
        holidays = holidays.concat(Holidays2)
        // ลบรายการวันหยุดที่ซ้ำกัน
        holidays = _.uniqBy(holidays, (e) => e.HolidayDate)
        // set data to store
        _this.$store.commit(`${SCHEDULE_MODULE_NAME}/${SET_HOLIDAY_ITEMS}`, holidays)
        // stop loading
        _this.setLoading(false)
      } catch (error) {
        // stop loading
        _this.setLoading(false)
        // notify error message
        _this.$errorHandler(error)
      }
    },
    /**
     * งานบริการ
     */
    async fetchDataServices() {
      const _this = this
      try {
        _this.setLoading(true)
        const services = await _this.$apiSetting.getAllService()
        this.$store.commit(`${SCHEDULE_MODULE_NAME}/${SET_SERVICE_ITEMS}`, services)
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    /* จุดบริการ */
    async fetchDataServicePoints() {
      const _this = this
      try {
        _this.setLoading(true)
        const rows = await _this.$apiSetting.getServicePoints()
        _this.$store.commit(`${SCHEDULE_MODULE_NAME}/${SET_SERVICE_POINT_ITEMS}`, rows)
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    /**
     * ข้อมูลตารางแพทย์
     */
    async fetchDataEvents(params) {
      const _this = this
      try {
        _this.setLoading(true)
        const events = await _this.$apiSetting.getDoctorScheduleEvents({
          start: _.get(params, 'start', ''),
          end: _.get(params, 'end', ''),
        })
        this.$store.commit(`${SCHEDULE_MODULE_NAME}/${SET_EVENTS_ITEMS}`, events)
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    convertXmlObject(data) {
      const XmlNode = new DOMParser().parseFromString(data, 'text/xml')
      const json = this.$xmlToJson(XmlNode)
      return _.get(json, 'response.data.HolidayData', [])
    },
    exportToExcel() {
      const _this = this
      const calendarApi = _this.$refs.fullCalendar.getApi()
      const events = calendarApi.getEvents().map(function (event) {
        const props = event.extendedProps
        let start = event.start ? moment(event.start).format('HH:mm') : ''
        let end = event.end ? moment(event.end).format('HH:mm') : ''
        if (event.allDay) {
          start = '00:00'
          end = '23:59'
        }
        return {
          กลุ่มงานบริการ: _.get(props, ['service_group_name'], '-'),
          แพทย์: _.get(props, ['doctor_fullname'], '-'),
          วันที่: moment(event.start).format('DD MMM YYYY'),
          เวลา: `${start}-${end} น.`,
          รายละเอียด: event.title,
          จำนวนคิวที่รับ: _.get(props, ['queue_total_qty'], 0),
          จำนวนคิวที่ระบุแพทย์ได้: _.get(props, ['queue_assign_qty'], 0),
        }
      })
      const startDate = moment(calendarApi.view.currentStart).format('DD/MM/YYYY')
      const endDate = moment(calendarApi.view.currentEnd).format('DD/MM/YYYY')

      /* make the worksheet */
      const ws = XLSX.utils.json_to_sheet(events)

      /* add to workbook */
      const wb = XLSX.utils.book_new()
      XLSX.utils.book_append_sheet(wb, ws, 'รายงาน')

      /* generate an XLSX file */
      XLSX.writeFile(wb, `${startDate}-${endDate}.xlsx`)
    },
    reloadEvents() {
      const calendarApi = this.$refs.fullCalendar.getApi()
      const params = {
        start: moment(calendarApi.view.activeStart).format('YYYY-MM-DD'),
        end: moment(calendarApi.view.activeEnd).format('YYYY-MM-DD'),
      }
      this.fetchDataEvents(params)
    },
  },
}
export default mixins

import _ from 'lodash'
import { mapActions, mapState } from 'vuex'
import Swal from 'sweetalert2'
import picture from '@/assets/media/svg/avatars/001-boy.svg'
import * as template from '@/core/template'

const mixins = {
  computed: {
    ...mapState({
      currentTab: (state) => _.get(state.examination, 'currentTab', 'waiting'),
      serviceGroups: (state) => _.get(state.examination, 'service_groups', []),
      service_group_id: (state) => _.get(state.examination, 'service_group_id', ''),
      service_ids: (state) => _.get(state.examination, 'service_ids', []),
      service_point_id: (state) => _.get(state.examination, 'service_point_id', ''),
      servicePoints: (state) => _.get(state.examination, 'service_points', []),
      doctors: (state) => _.get(state.examination, 'doctors', []),
      confirmCallDialog: (state) => _.get(state.examination, 'confirmCallDialog', false),
      confirmHoldDialog: (state) => _.get(state.examination, 'confirmHoldDialog', false),
      user: (state) => _.get(state.auth, 'user', null),
      doctor_id: (state) => _.get(state.examination, 'doctor_id', ''),
    }),
    currentTabComponent() {
      return 'tab-' + this.currentTab.toLowerCase()
    },
    filteredData() {
      let data = []
      switch (this.currentTab) {
        case 'waiting':
          data = this.rowsWait
          break
        case 'call':
          data = this.rowsCall
          break
        case 'hold':
          data = this.rowsHold
          break
        default:
          data = []
          break
      }
      return data
    },
    countDataItems() {
      return {
        waiting: this.rowsWait.length,
        call: this.rowsCall.length,
        hold: this.rowsHold.length,
      }
    },
    // กลุ่มงานบริการที่เลือกตามการตั้งค่า
    serviceGroup() {
      return this.serviceGroups.find((item) => item.service_group_id === this.service_group_id)
    },
    // ชื่อกลุ่มงานบริการ
    serviceGroupName() {
      return _.get(this.serviceGroup, 'service_group_name', '')
    },
    // จุดบริการที่เลือกตามการตั้งค่า
    servicePoint() {
      return this.servicePoints.find((item) => item.service_point_id === this.service_point_id)
    },
    // ชื่อจุดบริการ
    servicePointName() {
      return _.get(this.servicePoint, 'service_point_description', '')
    },
  },
  mounted() {
    this.fetchData()
  },
  methods: {
    async fetchData() {
      await this.fetchDataServiceGroups()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataServicePoints()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataDoctors()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataWaiting()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataCalling()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataHold()
    },
    ...mapActions({
      setCurrentTab: 'examination/setCurrentTab',
      setServiceGroupItems: 'examination/setServiceGroupItems',
      setServicePointItems: 'examination/setServicePointItems',
      setDoctorItems: 'examination/setDoctorItems',
    }),
    // กลุ่มงานบริการ
    async fetchDataServiceGroups() {
      const _this = this
      if (_this.serviceGroups.length) return
      try {
        _this.setLoading(true)
        const serviceGroups = await _this.$apiKiosk.getServiceGroups()
        _this.setServiceGroupItems(serviceGroups)
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // ข้อมูลรายชื่อจุดบริการ
    async fetchDataServicePoints() {
      const _this = this
      if (_this.servicePoints.length) return
      try {
        _this.setLoading(true)
        const items = await _this.$apiKiosk.getServicePoints()
        _this.setServicePointItems(items)
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // คิวรอเรียก
    async fetchDataWaiting(newParams = {}) {
      const _this = this
      if (!_this.service_point_id || !_this.doctor_id) return
      try {
        _this.setLoading(true)
        let params = {
          service_group_id: this.service_group_id,
          service_point_id: this.service_point_id,
          doctor_id: this.doctor_id,
        }
        params = _this.$updateObject(params, newParams)
        const data = await _this.$apiExamination.getDataWaiting(params)
        _this.rowsWait = data
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // คิวกำลังเรียก
    async fetchDataCalling(params = {}) {
      const _this = this
      if (!_this.isReadySetting) return
      try {
        _this.setLoading(true)
        const defaultParams = {
          service_group_id: _this.service_group_id,
          doctor_id: this.doctor_id,
          service_point_id: _this.service_point_id,
          queue_status_id: 2, // default คิวกำลังเรียก
        }
        const dataParams = _this.$updateObject(defaultParams, params)
        const data = await _this.$apiExamination.getDataCalling(dataParams)

        // กำลังเรียก
        if (_.get(dataParams, ['caller_id'])) {
          _this.rowsCall = _this.rowsCall.concat(data)
        } else {
          _this.rowsCall = data
        }
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    // คิวพัก
    async fetchDataHold(params = {}) {
      const _this = this
      if (!_this.isReadySetting) return
      try {
        _this.setLoading(true)
        const defaultParams = {
          service_group_id: _this.service_group_id,
          doctor_id: this.doctor_id,
          service_point_id: _this.service_point_id,
          queue_status_id: 3, // default คิวพัก
        }
        const dataParams = _this.$updateObject(defaultParams, params)
        const data = await _this.$apiExamination.getDataCalling(dataParams)

        if (_.get(dataParams, ['caller_id'])) {
          _this.rowsHold = _this.rowsCall.concat(data)
        } else {
          _this.rowsHold = data
        }
        _this.setLoading(false)
      } catch (error) {
        _this.setLoading(false)
        _this.$errorHandler(error)
      }
    },
    /**
     * รายชื่อแพทย์ทั้งหมด
     */
    async fetchDataDoctors() {
      const _this = this
      try {
        const doctors = await _this.$apiExamination.getDoctors()
        _this.setDoctorItems(doctors)
      } catch (error) {
        _this.$errorHandler(error)
      }
    },
    // บันทึกการตั้งค่า
    async onSubmitFormSetting() {
      await this.fetchDataWaiting()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataCalling()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      await this.fetchDataHold()
      await new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, 500)
      })
      this.$toastr.success(`การตั้งค่าถูกบันทึก`, ``, {
        timeOut: 3000,
        positionClass: 'toast-top-right',
        progressBar: true,
        closeButton: true,
      })
    },
  },
}

export default mixins

import querystring from 'querystring'
import _ from 'lodash'
import Swal from 'sweetalert2'
import moment from 'moment'
import isEmpty from 'is-empty'
moment.locale('th')

export const toQueryString = (obj) => {
  /* const params =
    '?' +
    Object.keys(obj)
      .map((key) => `${key}=${obj[key] ? obj[key].toString() : ''}`)
      .join('&') */
  return `?${querystring.stringify(obj)}`
}

export const getErrorMessage = (error) => {
  const { message, errors } = error
  if (message) {
    return message
  } else if (errors) {
    return errors
  } else {
    return error
  }
}

export const errorHandler = (error, options = {}) => {
  const title = _.get(error, ['data', 'name']) || _.get(error, ['name']) || 'Oops..'
  Swal.fire(
    updateObject(
      {
        icon: 'error',
        title: title,
        text: getErrorMessage(error),
        timer: 3000,
        showConfirmButton: false,
      },
      options
    )
  )
}

export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties,
  }
}

export const groupBy = (array, key) => {
  const groups = array.reduce((r, a) => {
    r[a[key]] = [...(r[a[key]] || []), a]
    return r
  }, {})
  return groups
}

export const sortBy = (array, sortKey, order = 1) => {
  // 1 = asc, -1 = desc
  const sortOrders = array.slice().sort(function (a, b) {
    a = a[sortKey]
    b = b[sortKey]
    return (a === b ? 0 : a > b ? 1 : -1) * order
  })
  return sortOrders
}

export const mapDataOptions = (options = [], key, value) => {
  return options.map((row) => {
    return { id: row[key], text: row[value] }
  })
}

// Convert XML to JSON
export const xmlToJson = (xml) => {
  // Create the return object
  let obj = {}

  if (xml.nodeType === 1) {
    // element
    // do attributes
    if (xml.attributes.length > 0) {
      obj['@attributes'] = {}
      for (let j = 0; j < xml.attributes.length; j++) {
        const attribute = xml.attributes.item(j)
        obj['@attributes'][attribute.nodeName] = attribute.nodeValue
      }
    }
  } else if (xml.nodeType === 3) {
    // text
    obj = xml.nodeValue
  }

  // do children
  // If all text nodes inside, get concatenated text from them.
  const textNodes = [].slice.call(xml.childNodes).filter(function (node) {
    return node.nodeType === 3
  })
  if (xml.hasChildNodes() && xml.childNodes.length === textNodes.length) {
    obj = [].slice.call(xml.childNodes).reduce(function (text, node) {
      return text + node.nodeValue
    }, '')
  } else if (xml.hasChildNodes()) {
    for (let i = 0; i < xml.childNodes.length; i++) {
      const item = xml.childNodes.item(i)
      const nodeName = item.nodeName
      if (typeof obj[nodeName] === 'undefined') {
        obj[nodeName] = xmlToJson(item)
      } else {
        if (typeof obj[nodeName].push === 'undefined') {
          const old = obj[nodeName]
          obj[nodeName] = []
          obj[nodeName].push(old)
        }
        obj[nodeName].push(xmlToJson(item))
      }
    }
  }
  return obj
}

export const calculateAge = (patient) => {
  if (isEmpty(patient)) {
    return { years: 0, months: 0, days: 0 }
  } else if (patient.birthdate) {
    // const birthday = String(patient.birthdate).split('-')
    const today = moment().format('YYYY-MM-DD').split('-')
    const a = moment(today)
    const b = moment(patient.birthdate)

    const years = a.diff(b, 'year')
    b.add(years, 'years')

    const months = a.diff(b, 'months')
    b.add(months, 'months')

    const days = a.diff(b, 'days')
    return { years: years, months: months, days: days }
  } else {
    return { years: 0, months: 0, days: 0 }
  }
}

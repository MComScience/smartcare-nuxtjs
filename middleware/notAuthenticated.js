export default function ({ store, redirect }) {
  // If the user is authenticated redirect to home page
  const isAuthenticated = store.getters['auth/isAuthenticated']
  if (isAuthenticated) {
    return redirect('/')
  }
}

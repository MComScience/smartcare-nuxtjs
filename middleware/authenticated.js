import querystring from 'querystring'
export default function ({ store, redirect, route }) {
  const isAuthenticated = store.getters['auth/isAuthenticated']
  const userPermissions = store.getters['auth/userPermissions']
  const userRoles = store.getters['auth/userRoles']
  // const requiresAuth = route.meta.reduce((requiresAuth, meta) => meta.requiresAuth || requiresAuth, false)
  const query = querystring.stringify({ redirect: route.fullPath })
  // If the user is not authenticated
  if (!isAuthenticated) {
    return redirect(decodeURIComponent(`/auth/login?${query}`))
  } else if (!userPermissions.includes(String(route.path).replace('/en')) && !userRoles.includes('Admin')) {
    return redirect(decodeURIComponent(`403?${query}`))
  }
}

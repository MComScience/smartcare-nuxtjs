export default function ({ store, route }) {
  const breadcrumbs = route.meta.reduce((breadcrumbs, meta) => meta.breadcrumbs || breadcrumbs, [])
  store.dispatch('breadcrumbs/setBreadcrumb', breadcrumbs)
}

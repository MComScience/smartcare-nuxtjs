const devMode = process.env.NODE_ENV === 'development'

module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: ['@nuxtjs', 'prettier', 'prettier/vue', 'plugin:prettier/recommended', 'plugin:nuxt/recommended'],
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    'no-console': devMode ? 'off' : 'warn',
    'prettier/prettier': 'off',
    'nuxt/no-cjs-in-config': 'off',
    'vue/no-v-html': 'off',
    'no-unused-expression': 'off',
    'no-new': 0,
    'no-new-require': 'off',
    'no-unused-vars': devMode ? 'off' : 'warn',
    'no-throw-literal': 'off',
    'no-unreachable': 'off',
    'no-prototype-builtins': 'off',
    'no-undef': 'off',
    'no-case-declarations': 'off',
    'object-shorthand': 'off',
  },
}
